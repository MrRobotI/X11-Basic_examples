gm=0
i=1
WHILE len(PARAM$(i))
  i0=VAL(PARAM$(i))
  INC i
WEND
IF i0=0
  i0=tineget("GLOBALS[HECUR]")
  gm=1
ENDIF
alpha=5/30
t=0
dt=1/60/60

~@hochrechnung()

IF gm
  weiss=GET_COLOR(65535,65535,65535)
  grau=GET_COLOR(65535/1.2,65535/1.2,65535/1.2)
  schwarz=GET_COLOR(0,0,0)
  rot=GET_COLOR(65535,0,0)
  gelb=GET_COLOR(20000,65535,20000)
  groesse=0.7
  bw=700
  bh=200
  COLOR schwarz
  SIZEW ,bw,bh
  PAUSE 1
  PBOX 0,0,bw,bh
  COLOR rot
  DEFTEXT ,0.05,0.05
  LTEXT 10,100,"Run-Ende:"

  DO
    i0=tineget("GLOBALS[HECUR]")

    a=@hochrechnung()
    @display(unixtime$(timer+3600*a))

    PAUSE 3
  LOOP

ENDIF

QUIT

FUNCTION hochrechnung()
  tau=@tau(i0)
  i=i0
  t=0

  DO
    di=-i/tau*dt
    ADD i,di
    tau=@tau(i)
    ADD t,dt
    ' print t;" ";i;" ";tau
    IF i<13
      PRINT "13 mA erreicht in ";STR$(t,3,3);" Stunden, also um ";unixtime$(timer+t*3600)
      RETURN t
    ENDIF
    EXIT if t>10
  LOOP
ENDFUNCTION

FUNCTION i(t)
  RETURN i0*exp(-t/tau)
ENDFUNCTION
FUNCTION tau(i)
  RETURN 16-alpha*i
ENDFUNCTION
PROCEDURE display(d$)
  COLOR schwarz
  PBOX 0,0,bw,bh
  COLOR rot
  DEFLINE ,2,2
  DEFTEXT ,0.05,0.07
  LTEXT 50,2,"Run-Ende:"
  DEFLINE ,25*groesse,2
  DEFTEXT 1,groesse,2*groesse
  COLOR gelb

  'for i=0 to 360 step 10
  LTEXT bw/2+cos(i/180*pi)*10-LTEXTLEN(d$)/2,30+sin(i/180*pi)*10,d$
  'next i
  COLOR weiss
  LTEXT bw/2-LTEXTLEN(d$)/2,30,d$
  VSYNC
RETURN
