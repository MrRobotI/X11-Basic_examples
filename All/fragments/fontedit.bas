' This is a simple font edit tool for designing the
' line font, which is built in X11-Basic
' nothing special.  (c) Markus Hoffmann 2000
'
SIZEW ,4*100+40,4*100+40
SHOWPAGE
PAUSE 0.1
CLEARW
grau=GET_COLOR(20000,20000,20000)
schwarz=COLOR_RGB(0,0,0)
gelb=COLOR_RGB(1,1,0)
COLOR grau
BOX 0,0,4*100,4*100
LINE 0,4*50,4*100,4*50
FOR i=0 TO 100-1
  FOR j=0 TO 100-1
    PLOT 4*i+2,4*j+2
  NEXT j
NEXT i
COLOR gelb
@drawchar
SHOWPAGE
PAUSE 5
QUIT

PROCEDURE drawchar
  LOCAL x,y,ox,oy
  RESTORE
  READ width
  LINE 4*width,0,4*width,4*100
  READ x,y
  ox=x
  oy=y
  WHILE x>-1
    IF x>100
      SUB x,101
      ox=x
      oy=y
      count=0
    ENDIF
    LINE ox*4,oy*4,x*4,y*4
    ox=x
    oy=y
    INC count
    READ x,y
  WEND
  SHOWPAGE
RETURN

DATA 100,100,60,70,40,30,40,1,70,1,70,30,100,70,100,100,80,201,40,100,120,60,130,20,120,60,110,100,120,-1,-1

