' text_ana.bas (c) Markus Hoffmann 2002-05-05
' the original version was written in GFA-Basic for ATARI ST 1990 (?)
'
' 2016-01-13 little changes MH
'
c=0
weiss=COLOR_RGB(1,1,1)
schwarz=COLOR_RGB(0,0,0)
rot=COLOR_RGB(1,0,0)
gelb=COLOR_RGB(1,1,0)
COLOR schwarz,weiss

DIM c(256),b(256)
DO
  CLEARW
  zab=18
  y=80
  x=0
  ARRAYFILL b(),0
  ARRAYFILL c(),0
  t$="             Textanalysator   (c) Markus Hoffmann         V.1.00"
  t$=t$+"|"
  t$=t$+"|H�tten Sie gewu�t, da� Ihr ATARI ST ihre Briefe und Texte lesen |und verstehen kann ?"
  t$=t$+"|Nein? Dann lassen Sie sich �berzeugen..."
  t$=t$+"|Sie m�ssen zuerst dem Programm ein ASCII-Textfile zu lesen geben. |Dieser File sollte"
  t$=t$+"mindestens 3 KBytes lang sein und maximal 32 Kbytes."
  t$=t$+"|Nach dem Lesen, wird die statistische Buchstabenh�ufigkeit ermittelt| und grafisch angezeigt."
  t$=t$+"|Nach einem Tastendruck legt der Rechner mit der Inhaltsangabe los."
  t$=t$+"|Diese k�nnen Sie zus�tzlich auf dem Drucker ausgeben lassen."
  t$=t$+"|Durch den Intelligenzquozienten bestimmen Sie, wie verst�ndlich das| Resultat"
  t$=t$+"|sein soll. Hier empfiehlt sich aber eine m�glichst kleine Zahl |(z.b. 5), damit"
  t$=t$+"|das Resultat nicht zusehr dem Orginal �hnelt. Prinzipiell gilt:"
  t$=t$+"|Je gr��er der Textfile, desto h�her darf der Intelligenzquotient |sein."
  t$=t$+"|Optimal sind zum Beispiel 35000 Bytes Text und IQ=8"
  t$=t$+"|Viel Spass garantiert                       Markus Hoffmann"
  ALERT 0,t$,1," los ...",a
  TEXT 0,20,"Text laden:"
  FOR i=ASC("A") TO ASC("Z")
    TEXT (i-ASC("A"))*16,395,CHR$(i)+" "
  NEXT i
  TEXT (i-ASC("A"))*16,380,"� � � �  9"
  FOR i=0 TO 100 STEP 10
    LINE 0,383-i*383/100,639,383-i*383/100
  NEXT i
  FILESELECT ,"./*.txt","",f$
  IF NOT EXIST(f$)
    QUIT
  ENDIF
  TEXT 100,20,f$
  '
  OPEN "I",#1,f$
  l=LOF(#1)
  a=MALLOC(l)
  IF a<=0
    QUIT
  ENDIF
  BGET #1,a,l
  CLOSE
  '

  FOR i=0 TO l-1
    u=ASC(UPPER$(CHR$(PEEK(a+i))))
    c(u)=c(u)+1
    IF (i MOD 1000)=0
      COLOR weiss
      PBOX 15,25,180,43
      COLOR schwarz
      TEXT 20,40,"Lese: "+STR$(i)
      SHOWPAGE
    ENDIF
  NEXT i
  COLOR weiss
  PBOX 15,25,180,43
  COLOR schwarz
  TEXT 20,40,"Lese: "+STR$(i)
  nn:
  COLOR gelb
  v=l-c(32)-c(10)-c(13)
  FOR i=ASC("A") TO ASC("Z")
    PBOX (i-65)*16+1,383,(i-65)*16+8,383-((c(i)+c(i+ASC("a")-ASC("A")))/v)*383*4
  NEXT i
  PBOX 26*16+1,383,26*16+8,383-(c(ASC("�"))/v)*383*4
  PBOX 27*16+1,383,27*16+8,383-((c(ASC("�"))+c(ASC("�")))/v)*383*4
  PBOX 28*16+1,383,28*16+8,383-((c(ASC("�"))+c(ASC("�")))/v)*383*4
  PBOX 29*16+1,383,29*16+8,383-((c(ASC("�"))+c(ASC("�")))/v)*383*4
  CLR z
  FOR i=ASC("0") TO ASC("9")
    ADD z,c(i)
  NEXT i
  PBOX 30*16+9,383,30*16+8+8,383-(z/v)*383*4
  SHOWPAGE
  COLOR rot
  GPRINT "Interpretation:"
  GPRINT " Einen Moment bitte...";CHR$(13)'
  b$=SPACE$(32500)
  ' anz=C:ana(L:a,L:V:b$,L:l,L:LEN(b$))
  GPRINT "ANZ:"'anz
  b$=LEFT$(b$,anz)
  GPRINT "Len b$:"'LEN(b$)
  '
  t$="Bitte Intelligenzquotienten eingeben: (2-10) "+CHR$(27)+" 2"
  ALERT 0,t$,1," OK ",a,o$
  o=MAX(VAL(o$),2)
  '
  GPRINT
  GPRINT "Zufallstext Typ: 'AFFE' ";o;". Ordnung..."
  GPRINT "-----------------------------------------"
  as:
  ALERT 2,"Ausgabe auf ...                ",3,"Drucker|Datei|Screen",v
  IF v=1
    OPEN "O",#1,"/dev/lpt0"
  ELSE IF v=2
    FILESELECT "Ausgabe:","\*.*","",s$
    IF LEN(s$)=0
      GOTO as
    ENDIF
    OPEN "O",#1,s$
  ENDIF
  IF v=1
    IF OUT?(0)=0
      PRINT "Drucker nicht an, abgebrochen ..."
      v=3
    ENDIF
  ENDIF
  IF v<>3
    PRINT #1,"Zufallstext Typ: 'AFFE' ";o;". Ordnung..."
    PRINT #1,"nach"'f$
    PRINT #1,"-------------------------------------------"
    PRINT #1
  ENDIF
  CLR pos
  g$=" "
  DO
    ARRAYFILL b(),0
    CLR cc
    PRINT "CC="'cc,"POS="'pos
    PRINT "LEN(b$):"'LEN(b$)
    PRINT "LEN(g$):"'LEN(g$)
    DO
      pos=INSTR(b$,g$,pos+1)
      EXIT IF pos=0
      INC b(ASC(MID$(b$,pos+LEN(g$),1)))
      INC cc
      PRINT "ADD cc"
    LOOP
    PRINT "CC:"'cc
    r&=@zufall
    IF r&=0
      r&=ASC("0")
    ENDIF
    @outs(antikro,r&)
    IF v<>3
      OUT #1,r&
    ENDIF
    g$=g$+CHR$(r&)
    g$=RIGHT$(g$,o)
    EXIT IF LEN(INKEY$)
  LOOP
  CLOSE
  ALERT 2,"Nochmal denselben Text|verwenden ?",1,"Ja|Nein",vb
  IF vb=1
    GOTO nn
  ENDIF
  '
  GPRINT AT(1,3);"Reihenfolge:"''
  CLR g$
  c=0
  b(c)=0
  FOR i=ASC("A") TO ASC("Z")+1
    FOR j=ASC("A") TO ASC("Z")
      IF b(j)>b(c)
        c=j
      ENDIF
    NEXT j
    GPRINT CHR$(c)+",";
    g$=g$+CHR$(c)
    b(c)=0
  NEXT i
  GPRINT
  IF LEFT$(g$,5)="EN"
    GPRINT ">>DEUTSCH:"''
  ELSE IF LEFT$(g$,5)="ER"
    GPRINT ">>LISTE:"''
  ELSE IF LEFT$(g$,5)="ET"
    GPRINT ">>ENGLISCH:"''
  ENDIF
  IF LEFT$(g$,5)="ENIRT"
    GPRINT "Computer-Anleitung, Wissensch. Aufsatz"
  ELSE IF LEFT$(g$,5)="ERTAN"
    GPRINT ">>Adressenliste Deutschland"
  ELSE IF LEFT$(g$,5)="ENIRS"
    GPRINT ">>DEUTSCH Wissensch. Abhandlung"
  ELSE IF LEFT$(g$,5)="ENITR"
    GPRINT ">>Deutsch Aufsatz"
  ELSE IF LEFT$(g$,5)="ETOAN"
    GPRINT ">>ENGLISCH"
  ELSE IF LEFT$(g$,5)="ETONS"
    GPRINT ">>Basic-Listing (englisch)"
  ELSE IF LEFT$(g$,5)="ERNIA"
    GPRINT ">>(GFA)-Basic-Listing (englisch)"
  ELSE IF LEFT$(g$,5)="NIEDA"
    GPRINT ">>(GFA)-Basic-Unterroutine (englisch)"
  ELSE IF LEFT$(g$,5)="ENISR"
    GPRINT ">>Deutscher Lehrbuchtext"
  ELSE IF LEFT$(g$,5)="ENRIS"
    GPRINT ">>Deutscher Brief/Aufsatz"
  ELSE IF LEFT$(g$,5)="ENRDS"
    GPRINT ">>S�chsisch  Geschaeftsbrief/Aufsatz"
  ELSE
    GPRINT ">>???"
  ENDIF
  ~INP(-2)
  ~MFREE(a)
LOOP
FUNCTION zufall
  IF cc=0
    ~FORM_ALERT(1,"[3][Der Text ist auf das Ende gelaufen!  CC=0][Oh!]")
    CLR g$
    RETURN 32
  ENDIF
  z:
  zz=RANDOM(cc+1)
  IF zz=0
    GOTO z
  ENDIF
  CLR ii
  DO
    SUB zz,b(ii)
    IF zz<=0
      RETURN ii
    ENDIF
    INC ii
  LOOP
ENDFUNC

PROCEDURE outs(fontadr,asc)
  PRINT CHR$(asc);
RETURN
