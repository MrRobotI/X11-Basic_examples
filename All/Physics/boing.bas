' converted from boing.sdlbas
' 2015-07-25 MGA/B+ left mouse down, optional drag mouse, release,
' see spring action say, "boing"
'
xmax=640
ymax=400
' SIZEW 1,xmax,ymax
CLEARW 1
black=COLOR_RGB(0,0,0)
white=COLOR_RGB(1,1,1)
blue2=COLOR_RGB(0,1,1)
blue=COLOR_RGB(0,0,1)
SHOWPAGE
' setdisplay(xmax,ymax,32,1)
' autoback(0)
DIM s(4,2)
s(0,0)=0
s(0,1)=50
s(1,0)=0
s(1,1)=ymax-50
s(2,0)=xmax+30
s(2,1)=50
s(3,0)=xmax+25
s(3,1)=ymax-50
da=0.03
m=1     ! Mass of the body
dt=0.01 ! time step

CLR a,boingx,boingy
tx=xmax/2
ty=ymax/2
DO
  IF MOUSEK=1
    tx=MOUSEX+20
    ty=MOUSEY
  ENDIF
  fx=-(tx-xmax/2)*100
  fy=-(ty-ymax/2)*100
  x__=fx/m
  y__=fy/m
  x_=x__*dt+x_
  y_=y__*dt+y_
  tx=x_*dt+tx
  ty=y_*dt+ty
  COLOR black
  PBOX 0,10,xmax,14
  PBOX 0,15,xmax,19
  COLOR blue2
  PBOX xmax/2,10,xmax/2+x_/10,14
  PBOX xmax/2,15,xmax/2+y_/10,19
  a=0
  BOX 0,0,xmax,ymax
  FOR corner=0 TO 3
    s1x=s(corner,0)
    s1y=s(corner,1)
    dx=(tx-s1x)/2000
    dy=(ty-s1y)/2000
    x=tx-20
    y=ty
    FOR i=1 TO 2000
      sx=20*COS(a)+x
      sy=20*SIN(a)+y
      COLOR blue
      BOX sx,sy,sx+2,sy+2
      COLOR black
      PLOT sx,sy
      COLOR white
      PLOT sx,sy+2
      SUB x,dx
      SUB y,dy
      ADD a,da
    NEXT i
  NEXT corner
  FOR l=25 TO 1 STEP -4
    COLOR COLOR_RGB(0,0,(255-l*7)/256)
    PCIRCLE tx-20,ty,l
  NEXT l
  SHOWPAGE ! screenswap
  PAUSE 0.03
  CLEARW 1
  TEXT xmax/5,ymax/10,"BOING V.1. in X11-Basic"
LOOP
QUIT

