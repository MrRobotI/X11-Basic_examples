'
' Program for simulating the longitudinal phase space bunch rotation
' in Proton HF of accelerators            (c) Markus Hoffmann 03/2004
'

n=10000        ! number of particles to be tracked
umlaufe=4*1024 ! number of macro revolutions to be tracked
dtuml=0.000021 ! revolution time in sec
dt=dtuml*50    ! time resolution for integrator
bx=16       ! Picture area location
by=16
bw=5*128      ! and dimension
bh=3*128

demax=0.5e-3 ! vertikale Skalierung (Energieabweichung)

' HERA-p HF parameter

bl=1.6      ! bunch length in ns
dee=1e-4    ! rel Energieunschaerfe
hf=52000000

eweg=0.0000005 ! Energieverlust pro Umlauf

' Berechne fs

uhf1=100e3   ! 52 MHz Cavity voltage [V]
uhf2=0       ! 208 MHz Cavity voltage [V]

DIM y(n),x(n)
DIM datax(umlaufe),mittel(umlaufe)
DIM parms(10)
DIM bins(200)
ARRAYFILL mittel(),0
parms(0)=dt
parms(1)=eweg
parms(2)=uhf1
parms(3)=uhf2
PRINT "Initializing particle distribution ..."
@init_gauss(n)
' @init_random(n)
' @init_gauss(n/1.5)
SIZEW ,bw+2*bx,bh+2*by
BOUNDARY 0
schwarz=GET_COLOR(0,0,0)
weiss=GET_COLOR(65535,65535,65535)
grau=GET_COLOR(20000,20000,20000)
rot=GET_COLOR(65535,0,0)
gruen=GET_COLOR(0,65535,0)
gelb=GET_COLOR(65535,65535,0)
COLOR schwarz
PBOX bx,by,bx+bw,by+bh
COLOR grau
BOX bx-1,by-1,bx+bw,by+bh
IF not EXIST("./trackit.so")
  SYSTEM "gcc -O3 -shared -o trackit.so trackit.c"
ENDIF
LINK #11,"./trackit.so"
FOR mega=0 TO 15
  CLR count
  DO
    COLOR schwarz
    PBOX bx,by,bx+bw,by+bh
    COLOR rot
    TEXT bx+10,by+10,STR$(count)
    TEXT bx+bw-50,by+bh-10,"(c) 2004 MH"
    LINE bx+bw/2,by+bh-16,bx+bw/2+bw*hf*1e-9,by+bh-16
    LINE bx+bw/2,by+bh-16-2,bx+bw/2,by+bh-16+2
    LINE bx+bw/2+bw*hf*1e-9,by+bh-16-2,bx+bw/2+bw*hf*1e-9,by+bh-16+2
    TEXT bx+bw/2+bw*hf*1e-9/2-10,by+bh-10+8,"1 ns"
    TEXT bx+bw/2-10,by+10,STR$(demax)
    COLOR weiss
    meanx=0
    ~CALL(SYM_ADR(#11,"trackit2"),L:n,L:1,P:VARPTR(x(0)),P:VARPTR(y(0)),P:VARPTR(meanx),P:VARPTR(parms(0)))
    SCOPE y(),x(),1,bh/2/demax,by+bh/2,bw/2/PI,bx+bw/2,1
    ARRAYFILL bins(),0
    ~CALL(SYM_ADR(#11,"binit"),L:n,L:200,P:VARPTR(x(0)),P:VARPTR(bins(0)))
    SCOPE bins(),1,-5000/n,bh,1,bx
    SHOWPAGE
    PAUSE 0.01
    IF mega=2
      ADD uhf2,570e3/umlaufe
      parms(3)=uhf2
    ELSE if mega=0
      ' uhf2=570e3/bw*mousex
      ' uhf2=570e3+100e3*sin(count/4.5)
      IF count=1000
        uhf1=uhf1*5
        parms(2)=uhf1
      ELSE if count=1014
        uhf1=uhf1/5
        parms(2)=uhf1
      ELSE if count=1015
        ' stop
      ENDIF
    ENDIF
    datax(count)=meanx
    INC count
    EXIT if count>=umlaufe
  LOOP
  IF mega=0
    uhf2=0
  ELSE if mega=2
    uhf2=570e3
  ELSE if mega=3
    CLR uhf1,uhf2
  ELSE if mega=4
    FOR i=0 TO n-1
      y(i)=y(i)-5e-4
    NEXT i
  ENDIF
  parms(2)=uhf1
  parms(3)=uhf2
NEXT mega
UNLINK #11
RUN
QUIT
PROCEDURE init_gauss(n)
  ' initializes phase space with 2D gaussian distribution
  LOCAL i,o
  o=2*pi*bl*1e-9*hf/2
  FOR i=0 TO n-1
    y(i)=GASDEV()*dee/2
    x(i)=GASDEV()*o
  NEXT i
RETURN
PROCEDURE init_random(n)
  ' initializes phase space with 2D gaussian distribution
  LOCAL i,x,y
  FOR i=0 TO n-1
    x=(RND()-0.5)
    y=(RND()-0.5)
    WHILE sqrt(x*x+y*y)>0.5
      x=(RND()-0.5)
      y=(RND()-0.5)
    WEND
    x(i)=x*2
    y(i)=y/1000
  NEXT i
RETURN
