ECHO OFF
tunt$=CHR$(27)+"[1m"
tnorm$=CHR$(27)+"[m"
CLS
PRINT "   von Markus Hoffmann       07.05.1986"
PRINT "       *==> DURCH DIE W�STE <==*"
DIM a(10)
PRINT AT(5,9);tunt$+"SPIELANLEITUNG"+tnorm$
PRINT AT(7,9);"Sie m�ssen mit Ihrem Lastwagen von Lager 1 durch die W�ste zu"
PRINT "        Lager 10  gelangen. Sie  k�nnen  die  Zwischenstationen  dazu"
PRINT "        benutzen, Kanister ab- bzw. aufzuladen, denn auf dem Weg  von"
PRINT "        einem Lager zum n�chsten verbraucht der Lastwagen 1 Kanister."
PRINT "        Sie  k�nnen  jedoch nur eine  bestimmte Anzahl  von Kanistern"
PRINT "        mitnehmen.    Wenn  Sie  jedoch  pl�tzlich  mit  leerem  Tank"
PRINT "        dastehen, haben Sie leider verloren."
PRINT
' Spiel
DO
  CLR a(),t,n
  a(1)=80

  s=1
  b=1
  PRINT
  PRINT
  REPEAT
    PRINT "  1 - 5 Schwierigkeitsgrad: 1 leicht 5 schwer"
    PRINT "    0   Alle Schwierigkeitsstufen gestaffelt"
    PRINT "    8   Altes Spiel fortsetzten"
    PRINT "    9   Programmende"
    INPUT " Ihre Wahl: ";sc
  UNTIL sc>=0 AND sc<=5 OR sc=8 OR sc=9
  IF sc=9
    END
  ELSE if sc=8
    VARLOAD a(),m,t,b,sc,s,n
  ELSE IF sc=0 OR sc=1
    m=8
  ELSE IF sc=2
    m=7
  ELSE IF sc=3
    m=6
  ELSE IF sc=4
    m=5
  ELSE IF sc=5
    m=4
  ENDIF
  ' Runde
  DO
    DO
      CLS
      PRINT AT(1,1);"      Durch die Wueste        ein Denkspiel von Markus Hoffmann"
      PRINT AT(4,5);"Lager   :  1  2  3  4  5  6  7  8  9 10"
      PRINT at(20,40);"Ladekapazit�t:"'m
      PRINT at(20,2);"9 = Programmende"
      ' Step

      PRINT AT(5,5);"Kanister: ";
      FOR i=1 TO 10
        PRINT str$(a(i),2,2);" ";
      NEXT i
      PRINT
      x=11+(s-1)*3
      PRINT TAB(x+4);"^     "
      PRINT TAB(x+4);"|     "
      PRINT TAB(x);"Lastwagen                "
      PRINT TAB(x+4);t;"      "
      IF s=10
        PRINT "GESCHAFFT !"
        ' exit if TRUE
        GOTO ex
      ENDIF
      IF t=0 AND a(s)=0
        PRINT "Sie m�ssen leider laufen !"
        ' exit if true
        GOTO ex
      ENDIF
      PRINT at(14,2);"Ihre M�glichkeiten :"
      IF t>1
        PRINT AT(15,2);"1 = Kanister abladen"
      ELSE
        PRINT at(15,2);"                    "
      ENDIF
      IF a(s)>0
        PRINT AT(16,2);"2 = Kanister zuladen"
      ELSE
        PRINT at(16,2);"                    "
      ENDIF
      IF t>0
        PRINT AT(17,2);"3 = zum n�chsten Lager"
      ELSE
        PRINT at(17,2);"                      "
      ENDIF
      IF t>0 AND s>1
        PRINT AT(18,2);"4 = zum vorangehenden Lager"
      ELSE
        PRINT at(18,2);"                           "
      ENDIF

      ' Wahl
      REPEAT
        PRINT at(21,10);"    ";CHR$(13);
        INPUT "Ihre Wahl :",wahl
      UNTIL wahl<10 AND wahl>0

      ' ABLADEN
      IF t>1 AND wahl=1
        DO
          INPUT "Wieviele abladen :";a
          IF a>t
            PRINT "Sie k�nnen doch h�chstens"'t'"abladen !"
          ENDIF
          EXIT if a<=t AND a=INT(a) AND a>=0
          PRINT "Falsche Eingabe ! nocheinmal"
        LOOP
        a(s)=a(s)+a
        SUB t,a
      ENDIF
      ' AUFLADEN
      IF a(s)>0 AND wahl=2
        DO
          INPUT "Wieviele aufladen :";anz_auf
          IF anz_auf>m-t
            PRINT "Es ist nur Platz f�r "'m'"Kanister !"
          ENDIF
          EXIT if anz_auf>=0 AND anz_auf=INT(anz_auf) AND anz_auf<=a(s) AND anz_auf<=m-t
          PRINT "Falsche Eingabe ! nochmal!"
        LOOP
        ADD t,anz_auf
        a(s)=a(s)-anz_auf
      ENDIF
      ' N�chstes Lager
      IF t>0 AND wahl=3
        INC s
        DEC t
        INC n
      ENDIF
      ' Voriges Lager
      IF t>0 AND s>1 AND wahl=4
        DEC s
        DEC t
        INC n
      ENDIF
      ' Ende
      IF wahl=9
        INPUT "Wollen Sie den Spielstand abspeichern ? [J/N]";janee$
        IF left$(UPPER$(janee$))="J"
          ' varsave a(),m,t,b,sc,n
        ENDIF
        PRINT "Ich hoffe, Sie hatten viel Spa�. Bis zum naechsten mal ..."
        END
      ENDIF
    LOOP
    ' Spielende:
    PRINT
    PRINT "Sie haben"'n'"Kanister gebraucht,"
    a=a(2)+a(3)+a(4)+a(5)+a(6)+a(7)+a(8)+a(9)
    PRINT "brachten"'t'"mit und haben"'a'"in der Wueste gelassen."
    IF m=4 AND sc=0 AND s=10
      PRINT tab(12);"Herzlichen Gl�ckwunsch, machen Sie sich bereit,"
      PRINT tab(6);"die Lorbeeren zu empfangen:"
      PRINT " Das war eine wahre Meisterleistung."
      PRINT " T O L L ! ! !"
      EXIT if true
    ENDIF
    IF sc=0
      PRINT " N�chste Runde"
      PRINT " -------------"
      DEC m
      INC b
      PAUSE 1
    ENDIF
    EXIT if sc<>0
  LOOP
  ex:
  IF sc=0 AND t=0 AND a(s)=0
    PRINT " Sie sind bis zum"'b;"-ten Schwierigkeitsgrad gekommen."
  ENDIF
  INPUT "Wollen Sie noch einmal spielen ? [J/N]";o$
  EXIT IF LEFT$(UPPER$(o$),1)<>"J"
LOOP
END
REM ******* Markus Hoffmann *******
REM *******   7.5.1986      *******
