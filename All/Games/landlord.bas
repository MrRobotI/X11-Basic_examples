' This is an X11-Basic port of LAND/LORD, by No Time To Play,
' itself a clone of the classic game Hamurabi. Use as you wish.

RANDOMIZE

population%=100
land%=1000
grain%=2000+RANDOM(6)*100
starved%=10
immigrants%=5
rats%=200
planted%=300
yield%=RANDOM(5)+1
price%=RANDOM(6)+17
year%=1701
plague%=FALSE

SPEAK "Welcome!"

CLS
PRINT COLOR(33, 1)
PRINT "   No Time To Play    "
PRINT "       presents       "
PRINT COLOR(36, 1)
PRINT "=      ==   =  =  === "
'print "=     =  =  =  =  =  ="
PRINT "=     =  =  == =  =  ="
PRINT "=     ====  = ==  =  ="
'print "=     =  =  =  =  =  ="
print "====  =  =  =  =  === "
print color(32, 1)
print "=      ==   ===   === "
'print "=     =  =  =  =  =  ="
print "=     =  =  =  =  =  ="
print "=     =  =  ===   =  ="
'print "=     =  =  =  =  =  ="
print "====   ==   =  =  === "
print color(37, 0)

speak "Press Enter."
showk
input "Press Enter when ready", answer$

repeat
 gosub report
 
 if (year% / 10) = int(year% / 10)
   input "Should we go on? "; answer$
   answer$ = left$(answer$, 1)
   if answer$ = "n" or answer$ = "N"
     print "And so our story ends."
     end
   endif
 endif
 
 gosub instruct
 gosub advance
until population% < 1

print
print "Mylord, I have bad news. Everyone left us."
print "We're broke. Ruined."
print "If only you could try again..."
speak "Try again!"

end

procedure report
 print

 print "Mylord, I report: in year "; year%
 
 if starved% = 1
   print "1 worker ran away, and"
 else
   print starved%; " workers ran away, and"
 endif
 
 if immigrants% = 1
   print "1 moved to the plantation."
 else
   print immigrants%; " moved to the plantation."
 endif

 if plague%
   print "Dysentery killed half of them."
 endif
 
 print "The population is now "; population%; "."
 print "We harvested "; yield% * planted%; " bushels"
 print "at "; yield%; " bushels of grain / acre."
 print "Rats ate "; rats%; " bushels,"
 print "leaving "; grain%; " in the barns."
 print "We own "; land%; " acres of land."
 print "Land is "; price%; " bushels per acre."
return

procedure instruct
 do
   input "How much land to buy? ", buying%
   let buying% = abs(buying%)
   if buying% * price% <= grain%
     exit
   else
     print "But we don't have enough grain!"
   endif
 loop
 if buying% > 0
   let land% = land% + buying%
   let grain% = grain% - buying% * price%
   print "Very well, we are left with "; grain%; " bushels."
 endif

 do
   input "How much land to sell? ", selling%
   let selling% = abs(selling%)
   if selling% <= land%
     exit
   else
     print "But we only have "; land%; " acres!"
   endif
 loop
 if selling% > 0
   let land% = land% - selling%
   let grain% = grain% + selling% * price%
   print "Very well, we now have "; grain%; " bushels."
 endif

 do
   input "Grain to pay workers in? ", fed%
   let fed% = abs(fed%)
   if fed% <= grain%
     exit
   else
     print "But we only have "; grain%; " bushels!"
   endif
 loop
 let grain% = grain% - fed%
 
 do
   input "How much land to seed? ", planted%
   let planted% = abs(planted%)
   if planted% > land%
     print "But we only have "; land%; " acres!"
   else if planted% > grain% * 2
     print "But we only have "; grain%; " bushels!"
   else if planted% > population% * 10
     print "But we only have "; population%; " people!"
   else
     exit
   endif
 loop
return

procedure advance
 let yield% = random(5) + 1
 let grain% = grain% + yield% * planted%
 let rats% = int(rnd() * (grain% * 0.07))
 let grain% = grain% - rats%
 let starved% = population% - int(fed% / 20)
 if starved% < 0
   let starved% = 0
 endif
 let population% = population% - starved%
 if population% > 0
   let immigrants1% = int(starved% / 2)
   let immigrants2% = int((5 - yield%) * grain% / 600 + 1)
   let immigrants% = immigrants1% + immigrants2%
    if immigrants%>50
      immigrants%=50
    ELSE IF immigrants%<0
      CLR immigrants%
    ENDIF
    ADD population%,immigrants%
    plague%=RND()<(population%/land%)
    IF plague%
      population%=INT(population%/2)
    ENDIF
  ENDIF
  price%=RANDOM(6)+17
  INC year%
RETURN
