' output of inline.bas for X11-Basic 07.04.2011
' contrib/BALLER.MUS 5084 Bytes. (compressed: 983 Bytes, 19%)
BALLER$=""
BALLER$=BALLER$+"WM(B*Y7_\bH@^'.TJ$WHK\;5DX*bT]`/_Cc4^C_+ZS.CLV7KSTEaJVS>*Z-L=cHY"
BALLER$=BALLER$+"V==N,aX:MBQCbI60]E$;%_%]&C^$2ZBB@@TIDJ/E*^QEbaX3L.AS2U6[EKQKcJSS"
BALLER$=BALLER$+"Q4\FX.?OMRbUL;V[F.66c0UI*XT)[bR]6*L/6W0b./G5`^]%\b[-?1O93BLYD\[P"
BALLER$=BALLER$+"^2(VQ`-]&4M@%2_P''`P-_%0/\aO+@DEU1M]7cJC&/>K+^+3I$F7=E@];?G.I$HK"
BALLER$=BALLER$+"\IZ.F@WcBDP8^,KFGA'W+CH[N2%'@='*($N3B/:RH0=^`0[._D2$XCa?]c1QO-$7"
BALLER$=BALLER$+"<2$XF:-N01IL4I=S9P03'_b$RbYG(c2TGY0<E0[9::[^5V`a<&EG=7L=1N[8c\HW"
BALLER$=BALLER$+"$XADAYG>>I;).GW9\Q5&0X82^_+':%&@`L-c%8WFP0U3CVPA+ZaW6:O^A7$;`>X1"
BALLER$=BALLER$+"M]:Y[?TMF&Fc%(<UA4A/N]QA>_DPPXXb4G`]^:U]SZ-P\0,Q@O*,9b8%LEBP^,Y$"
BALLER$=BALLER$+"[\OS0'O=P/=c%*3)5`8)7@/UN*54LUKL8VU\<S,ba5'ZO&@A@B*&S[2>;A)3.E^X"
BALLER$=BALLER$+"T`9KO>SK8'*$^7/Rb,*H*Fa8:5bLU$$`?RPL5[WbFA$GbOHG:4A9&7C\EH-KQ-/c"
BALLER$=BALLER$+"519^:@3Q$bD]*QA(](;2C53?.-],c)&'_Qa,Z7`Q$^.0*:3V=QH*ORC/9R-Lc)<*"
BALLER$=BALLER$+"P$5LC^_;OXW,'A%%b]@V<RNLL[G[7[5\NYDZ1;(Ec9GJV08?M'^NAQKDQS+-`AD0"
BALLER$=BALLER$+"ZTAZ5J]V9_=?[OX9&Y:GP&PL(FBI2W/I__@T$25<&RQ&G>%HH-SCN])T]T`QPY&A"
BALLER$=BALLER$+"PDDN7&9>7M>-?5XAHQ;7Ua[WC>H:TUFQ=-?*EO5\'`I-R[-G.X@%a2.6,\QGE=JN"
BALLER$=BALLER$+"V,MC]AEY00a::JV?5E]`/N_)1('JU4[YP])G@I7;@):O*_SV]8BG*?MNK;5ZB9T,"
BALLER$=BALLER$+"Q_XK<^*ZD$GP0b_.'Kb[?@WOADc?5OZS9L.%T8?*<'a5IP;P0JNL1Y51Q5H+'AJ)"
BALLER$=BALLER$+".[QZB1C\c`V?YKRXJb*IJHWG3aK&2Q>RU'004SR@?/@69,GNE;;:\KX_&L-M).7U"
BALLER$=BALLER$+"GL-55]F-Z^:1JNI9:^OEJ3Q5MKE>9:5DL20H7PN\J?LIW25V.I,=^-3KOT7H1HP$"
BALLER$=BALLER$+">9L3-R?[+MWcN4M/J655OWJ5ZNHL`N2Q7^N>K)NW]Z.DTZ-'$C)SA9:@(@G+I9'="
BALLER$=BALLER$+"c?c^\+B&^U?D\A=I_&1P^7C2C-.Q)5:JDEX(5IUUQ).$D6W6S2N=QRCZE;(X4>;N"
BALLER$=BALLER$+"HQB\?QcbS5*,WB\E0O&>a/X-Q:@%'$D$"
BALLER_MUS$=UNCOMPRESS$(INLINE$(BALLER$))

st_ton()=[0,1,3,5,7,8,10,12,13,15,17,19,20,22,24,25,27,29,31,32,34,36,37,39,41, 43, 44, 46, 48, 49, 51, 53,55, 56, 58, 60, 61, 63, 65, 67, 68, 70, 72, 73,75, 77, 79, 80, 82, 84, 85, 87, 89, 91, 92, 94, 96]
st_wert()=[15,16,17,18,19, 20, 21, 22, 24, 25, 27, 28, 30,32, 34, 36, 38, 40, 42, 45, 47, 50, 53, 56, 60,63, 67, 71, 75, 80, 84, 89, 95, 100, 106, 113, 119,127, 134, 142, 150, 159, 169, 179, 190, 201, 213, 226, 239,253, 268, 284, 301, 319, 338, 358, 379, 402, 426, 451, 478,506, 536, 568, 602, 638, 676, 716, 759, 804, 852, 903, 956,1013, 1073, 1136, 1204, 1276, 1351, 1432, 1517, 1607, 1703, 1804, 1911,2025, 2145, 2273, 2408, 2553, 2703, 2864, 3034, 3214, 3405, 3608, 3823]

@m_laden("baller.mus")
@m_musik
QUIT

' /******************** Laden und Speicher reservieren *************************/
PROCEDURE m_laden(file$)
  OPEN "I",#1,file$
  buffer$=input$(#1,16)
  MEMDUMP varptr(buffer$),16
  tempo=@short(buffer$,2)
  max_abl=@short(buffer$,3)
  max_tkt=@short(buffer$,4)
  walz=@short(buffer$,7)
  PRINT "tempo  =";tempo
  PRINT "max_abl=";max_abl
  PRINT "max_tkt=";max_tkt
  PRINT "walz   =";walz

  IF walz
    w_len=72
  ELSE
    w_len=96
  ENDIF

  '  liste=( short *)malloc( max_abl+2<<3 );  /*Statt der Lattice Funktion kann*/
  '   /* auch die Betriebssystemroutine  Malloc(...) verwandt werden */
  '  takte=( unsigned short *)calloc( w_len*2, max_tkt+2 ); /* Reserviert Spei-*/
  '   /* cherbereich der Gr��e (w_len*2)*(max_tkt+2) und l�scht ihn. */

  buffer$=input$(#1,36)
  PRINT "buffer"
  MEMDUMP varptr(buffer$),36
  liste$=input$(#1,(max_abl+1)*8)
  PRINT "liste"
  MEMDUMP varptr(liste$),(max_abl+1)*8
  takte$=input$(#1,(max_tkt+1)*w_len*2)
  PRINT "takte"
  MEMDUMP varptr(takte$),(max_tkt+1)*w_len*2)

  '  Fread( f_handle, 36, buffer );
  '  Fread( f_handle, max_abl+1<<3, liste );
  '  Fread( f_handle, (max_tkt+1)*w_len*2, &takte[100] );
  CLOSE #1
RETURN

FUNCTION short(a$,n)
  RETURN (PEEK(VARPTR(a$)+2*n)*256 AND -256)+(PEEK(VARPTR(a$)+2*n+1) AND 255)
ENDFUNCTION

' /************************ St�ck spielen **************************************/
PROCEDURE m_musik
  LOCAL buf_ptr,lis_ptr,ende,help
  LOCAL kan1,kan2,kan3,rau
  DIM tra(3)
  DIM lau(3)
  @s_init
  buffer[0]=0; buffer[1]=-2;
  buf_ptr=2
  lis_ptr=0
  temp=100
  lau(0)=0
  lau(1)=0
  lau(2)=100
  tra(0)=0
  tra(1)=0
  tra(2)=0
  ende=0

  WHILE not ende
    d=@short(liste$,lis_ptr)
    PRINT "d=";d,"lis_ptr=";lis_ptr
    IF d=-1
      DEC buf_ptr
      IF @short(buffer$,buf_ptr)=-2
        ende=1
      ENDIF
      DEC buf_ptr
      lis_ptr=@short(buffer$,buf_ptr)
    ELSE if d=-2
      INC lis_ptr
      tra(0)=@short(liste$,lis_ptr)
      INC lis_ptr
      tra(1)=@short(liste$,lis_ptr)
      INC lis_ptr
      tra(2)=@short(liste$,lis_ptr)
      INC lis_ptr
    ELSE if d=-3
      INC lis_ptr
      lau(0)=@short(liste$,lis_ptr)
      INC lis_ptr
      lau(1)=@short(liste$,lis_ptr)
      INC lis_ptr
      lau(2)=@short(liste$,lis_ptr)
      INC lis_ptr
    ELSE if d=-4
      INC lis_ptr
      temp=@short(liste$,lis_ptr)
      ADD lis_ptr,3
    ELSE if d=-5
      ADD lis_ptr,4
      POKE varptr(buffer)+2*buf_ptr,lis_ptr DIV 256
      POKE varptr(buffer)+2*buf_ptr+1,lis_ptr AND 255
      INC buf_ptr
      DPOKE varptr(buffer)+2*buf_ptr,0
      INC buf_ptr
    ELSE if d=-6
      ADD lis_ptr,4
      IF @short(buffer$,buf_ptr-1)=@short(liste$,lis_ptr-3)
        SUB buf_ptr,2
        help=0
        WHILE help>=0
          IF @short(liste$,lis_ptr)=-1
            help=-1
          ENDIF
          ADD help,abs(@short(liste$,lis_ptr)=-5)-abs(@short(liste$,lis_ptr)=-7)
          ADD lis_ptr,4
        WEND
      ENDIF
    ELSE if d=-7
    CASE -7: buffer[buf_ptr-1]++;
      lis_ptr=buffer[buf_ptr-2];
    ELSE if d=-8
      INC lis_ptr
      IF @short(liste$,lis_ptr)<0
        ADD lis_ptr,3
      ELSE
        buffer[buf_ptr++]=lis_ptr+3;
        buffer[buf_ptr++]=-5;
        lis_ptr=(liste[lis_ptr]-1)*4
      ENDIF
    ELSE
      rau=abs(@short(liste$,lis_ptr)>=0)*((@short(liste$,lis_ptr))*w_len+100)
      INC lis_ptr
      kan1=abs(@short(liste$,lis_ptr)>=0)*((@short(liste$,lis_ptr))*w_len+100)
      INC lis_ptr
      kan2=abs(@short(liste$,lis_ptr)>=0)*((@short(liste$,lis_ptr))*w_len+100)
      INC lis_ptr
      kan3=abs(@short(liste$,lis_ptr)>=0)*((@short(liste$,lis_ptr))*w_len+100)
      INC lis_ptr
      FOR help=0 TO w_len-1
        @s_rausch(@short(takte$,rau) and 255)
        INC rau
        kanal=0
        @s_note(@short(takte$,kan1))
        INC kan1
        INC kanal
        @s_note(@short(takte$,kan2))
        INC kan2
        INC kanal
        @s_note(@short(takte$,kan3))
        INC kan3
        @m_wait
        IF inp?(-2)
          help=100
          INC ende  ! Bei Taste Abbruch
        ENDIF
      NEXT help
    ENDIF
  WEND
  @s_quit
RETURN

PROCEDURE m_wait
  @m_wloop
RETURN

' /************* Standardroutine f�r m_wait(): Pause machen ********************/
PROCEDURE m_wloop
  PAUSE temp*tempo/2000/2000
RETURN

' /*****************************************************************************
'  * Und nun die Routinen zur eigentlichen Tonerzeugung; sie brauchen von      *
'  * Ihrem Programm nicht aufgerufen zu werden.                                */
' /****************************** Note spielen *********************************/
PROCEDURE s_note(wert)
  LOCAL ton_nr
  ton_nr=st_ton((wert/4) AND 63)
  IF wert AND 0x4000
    DEC ton_nr
  ENDIF
  IF wert AND 0x8000
    INC ton_nr
  ENDIF
  IF (wert AND 0xc000)=0xc000
    @s_rausch(wert and 255)
  ELSE
    SUB ton_nr,tra(kanal)
    IF ton_nr<0
      ton_nr=0
    ENDIF
    IF ton_nr>95
      ton_nr=95
    ENDIF
    @s_freq((st_wert(ton_nr)*(4-(wert and 3))+st_wert(ton_nr+1)*(wert and 3)) div 4)
    IF wert AND 0x1000
      @s_t_an
    ELSE
      @s_t_aus
    ENDIF
    IF wert AND 0x2000
      @s_r_an
    ELSE
      @s_r_aus
    ENDIF
    @s_laut(((wert div 256) and 15)*lau(kanal)/100)
  ENDIF
RETURN

' /*************************** Sound Ansteuerung *******************************/
' /**************************** Initialisierung ********************************/
PROCEDURE s_init
  @s_rausch(0)
RETURN
' /*********************** Wiederherstellen vor Ende ***************************/
PROCEDURE s_quit
RETURN
' ************************** Tonfrequenz setzen *******************************/
PROCEDURE s_freq(freq)
  PRINT "Tonfreq=";freq;" Kanal=";kanal
  IF kanal=0
    freq1=freq
  ELSE if kanal=1
    freq2=freq
  ELSE
    freq3=freq
  ENDIF
RETURN

' /************************** Lautst�rke setzen ********************************/
PROCEDURE s_laut(laut)
  PRINT "Lautstaerke=";laut;" Kanal=";kanal
  IF kanal=0
    laut1=laut
  ELSE if kanal=1
    laut2=laut
  ELSE
    laut3=laut
  ENDIF
RETURN

' /************************* Rauschperiode setzen ******************************/
PROCEDURE s_rausch(periode)
  PRINT "Rauschperiode=";periode
RETURN

/**************************** Ton einschalten ********************************/
PROCEDURE s_t_an
  PRINT "ton ein, kanal=";kanal
  IF kanal=0
    IF laut1>5
      SOUND freq1
    ENDIF
    PAUSE 0.0
  ELSE if kanal=1
    IF laut2>5
      SOUND freq2
    ENDIF
    PAUSE 0.0
  ELSE
    IF laut3>5
      SOUND freq3
    ENDIF
    PAUSE 0.0
  ENDIF
RETURN

/**************************** Ton ausschalten ********************************/
PROCEDURE s_t_aus
  PRINT "ton aus, kanal=";kanal
  IF kanal=0
    ' sound 0
  ELSE if kanal=1
    ' sound 0
  ELSE
    ' sound 0
  ENDIF
RETURN

' /*************************** Rauschen einschalten ****************************/
PROCEDURE s_r_an
  PRINT "rausch ein, kanal=";kanal
RETURN

' /*************************** Rauschen ausschalten ****************************/
PROCEDURE s_r_aus
  PRINT "rausch aus, kanal=";kanal
RETURN

' /*************************** Sound abschalten ********************************/
PROCEDURE s_aus
  kanal=0
  @s_laut(0)
  kanal=1
  @s_laut(0)
  kanal=2
  @s_laut(0)
RETURN
