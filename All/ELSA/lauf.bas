' Kleine Laufschrift auf dem Keithley im Bedienungsraum
' Markus Hoffmann 1998

ECHO off
CLR i
a$="GUTEN@TAG,@FROHES@SCHAFFEN@@@"

DO
  t$=MID$(a$+a$,i MOD LEN(a$),10)
  CSPUT "ELS_DIAG_TOROID.IECONTROL_SC","D"+t$+"X"
  PAUSE 0.5
  PRINT t$+CHR$(13);
  FLUSH
  INC i
LOOP

QUIT
