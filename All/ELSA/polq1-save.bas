'
' Speichert die Einstellungen fuer die Quelle 1 ab.
' Letzte Bearbeitung: Markus Hoffmann Sept. 1999
'
ECHO off
dir$="/sgt/elsa/data/polq1/"
file$=dir$+"data.dat"

IF exist(file$)
  SYSTEM "cp "+file$+" "+file$+".save."+date$+"_"+time$
ENDIF

OPEN "O",#1,file$
PRINT #1,"#  Quelleneinstellungen vom "+date$+"  "+time$
PRINT #1,"#"

READ a$
WHILE a$<>"***"
  PRINT #1,a$+",   "+STR$(csget(a$))
  READ a$
WEND

CLOSE #1
QUIT

heinzinger:
DATA "POL_SOURCE1_NG1_1.STROM_AC"
DATA "POL_SOURCE1_NG1_2.STROM_AC"
DATA "POL_SOURCE1_NG1_3.STROM_AC"
DATA "POL_SOURCE1_NG1_4.STROM_AC"
DATA "POL_SOURCE1_NG1_5.STROM_AC"
DATA "POL_SOURCE1_NG1_6.STROM_AC"
DATA "POL_SOURCE1_NG1_7.STROM_AC"
DATA "POL_SOURCE1_NG2_1.STROM_AC"
DATA "POL_SOURCE1_NG2_2.STROM_AC"
DATA "POL_SOURCE1_NG2_3.STROM_AC"
DATA "POL_SOURCE1_NG2_4.STROM_AC"
DATA "POL_SOURCE1_NG2_5.STROM_AC"
DATA "POL_SOURCE1_NG2_6.STROM_AC"
cops:
DATA "POL_SOURCE1_COPS1.POL1_DC"
DATA "POL_SOURCE1_COPS1.STROM1_AC"
DATA "POL_SOURCE1_COPS1.POL2_DC"
DATA "POL_SOURCE1_COPS1.STROM2_AC"
DATA "POL_SOURCE1_COPS1.POL3_DC"
DATA "POL_SOURCE1_COPS1.STROM3_AC"
DATA "POL_SOURCE1_COPS1.POL4_DC"
DATA "POL_SOURCE1_COPS1.STROM4_AC"
DATA "POL_SOURCE1_COPS1.POL5_DC"
DATA "POL_SOURCE1_COPS1.STROM5_AC"
DATA "POL_SOURCE1_COPS1.POL6_DC"
DATA "POL_SOURCE1_COPS1.STROM6_AC"
DATA "POL_SOURCE1_COPS1.POL7_DC"
DATA "POL_SOURCE1_COPS1.STROM7_AC"
DATA "POL_SOURCE1_COPS1.POL8_DC"
DATA "POL_SOURCE1_COPS1.STROM8_AC"
DATA "POL_SOURCE1_COPS2.POL1_DC"
DATA "POL_SOURCE1_COPS2.STROM1_AC"
DATA "POL_SOURCE1_COPS2.POL2_DC"
DATA "POL_SOURCE1_COPS2.STROM2_AC"
DATA "POL_SOURCE1_COPS2.POL3_DC"
DATA "POL_SOURCE1_COPS2.STROM3_AC"
DATA "POL_SOURCE1_COPS2.POL4_DC"
DATA "POL_SOURCE1_COPS2.STROM4_AC"
DATA "POL_SOURCE1_COPS2.POL5_DC"
DATA "POL_SOURCE1_COPS2.STROM5_AC"
DATA "POL_SOURCE1_COPS2.POL6_DC"
DATA "POL_SOURCE1_COPS2.STROM6_AC"
DATA "POL_SOURCE1_COPS2.POL7_DC"
DATA "POL_SOURCE1_COPS2.STROM7_AC"
DATA "POL_SOURCE1_COPS2.POL8_DC"
DATA "POL_SOURCE1_COPS2.STROM8_AC"
DATA "POL_SOURCE1_COPS3.POL1_DC"
DATA "POL_SOURCE1_COPS3.STROM1_AC"
DATA "POL_SOURCE1_COPS3.POL2_DC"
DATA "POL_SOURCE1_COPS3.STROM2_AC"
DATA "POL_SOURCE1_COPS3.POL3_DC"
DATA "POL_SOURCE1_COPS3.STROM3_AC"
DATA "POL_SOURCE1_COPS3.POL4_DC"
DATA "POL_SOURCE1_COPS3.STROM4_AC"
DATA "POL_SOURCE1_COPS3.POL5_DC"
DATA "POL_SOURCE1_COPS3.STROM5_AC"
DATA "POL_SOURCE1_COPS3.POL6_DC"
DATA "POL_SOURCE1_COPS3.STROM6_AC"
DATA "POL_SOURCE1_COPS3.POL7_DC"
DATA "POL_SOURCE1_COPS3.STROM7_AC"
DATA "POL_SOURCE1_COPS3.POL8_DC"
DATA "POL_SOURCE1_COPS3.STROM8_AC"
DATA "POL_SOURCE1_COPS4.POL1_DC"
DATA "POL_SOURCE1_COPS4.STROM1_AC"
DATA "POL_SOURCE1_COPS4.POL2_DC"
DATA "POL_SOURCE1_COPS4.STROM2_AC"
DATA "POL_SOURCE1_COPS4.POL3_DC"
DATA "POL_SOURCE1_COPS4.STROM3_AC"
DATA "POL_SOURCE1_COPS4.POL4_DC"
DATA "POL_SOURCE1_COPS4.STROM4_AC"
DATA "POL_SOURCE1_COPS4.POL5_DC"
DATA "POL_SOURCE1_COPS4.STROM5_AC"
DATA "POL_SOURCE1_COPS4.POL6_DC"
DATA "POL_SOURCE1_COPS4.STROM6_AC"
DATA "POL_SOURCE1_COPS4.POL7_DC"
DATA "POL_SOURCE1_COPS4.STROM7_AC"
DATA "POL_SOURCE1_COPS4.POL8_DC"
DATA "POL_SOURCE1_COPS4.STROM8_AC"
laser:
DATA "POL_LASER_STEUERUNG.VOLTAGE_AC"
DATA "POL_LASER_STEUERUNG.VOLTAGEMIN_AC"
DATA "POL_LASER_STEUERUNG.VOLTAGEMAX_AC"

DATA "***"
