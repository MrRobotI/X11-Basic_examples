' Wobbel.bas    fuer die 120 kV Quelle
' Markus Hoffmann im Nov. 1998

' goto wobbel2
i=0
ECHO off
parameter$="EXT_MAGNETE_QD1.STROM_AC"

sollwert=50
amplitude=45

DO

  ww=sollwert+amplitude*sin(i/3)

  PRINT space$((ww-sollwert)/amplitude*40+40)+"*"

  CSPUT parameter$,ww
  IF sin(i/3)<-0.9
    ' beep
  ENDIF
  PAUSE 0.2
  INC i
LOOP
QUIT

wobbel2:

ECHO off
sollwert1=1.093
ampl1=0.01
sollwert2=0.3
ampl2=0.3
i=0
PRINT "Dipol Wobbelt ..."
DO
  INC i
  ww1=ampl1*sin(i/20)+sollwert1
  ' ww2=-ampl2*sin(i/8)+sollwert2
  PRINT space$((ampl1-sollwert1+ww1)*40/ampl1);"#"

  ' print ww1,ww2
  CSSET "POL_SOURCE1_NG2_4.STROM_AC",ww1
  ' csset "POL_SOURCE1_NG1_7.STROM_AC",ww2
  PAUSE 0.01
LOOP
