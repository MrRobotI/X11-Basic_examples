ECHO off
'
' stromueberwachung.bas
' M. Hoffmann & H. Bongartz
'
' $Header: stromueberwachung.bas,v 1.9 2000/02/18 17:08:35 control Exp $
'
' Warngrenze in Prozent
grenze=30
wartedelay=5
warten=wartedelay
offset=0
'a=1000*CSGET("STE_DIAG_TOROID.TRANSQ_AM")
a=CSGET("SUP_COUNTER.GDH_STROM_AM")
CLR count
CLR alarm
CLR ucount
CLR ualarm
CLR flag
CLR lflag
sleeptime=3
' Soundausgabe an per Default
nosound=0
' Kein Sound bei Highscore per Default
hssound=0
' Wenn 1, pausiere Ausgabe
CLR pause
' Wenn 1, fester Bezugswert
CLR fix
' Wenn 1, warnen bei Ueberschreiten
CLR ueber
dipolold$="DIPOLE: Netzgeraet OK !"
@help
DO
  IF pause=0
    PAUSE sleeptime
    ' b=1000*CSGET("STE_DIAG_TOROID.TRANSQ_AM")
    b=CSGET("SUP_COUNTER.GDH_STROM_AM")
    b=b-offset
    ' print @fstr$(a,2)+" pC    "+@fstr$(b,2)+" pC"
    PRINT @fstr$(a,2)+" pA    "+@fstr$(b,2)+" pA"
    IF b<(1-grenze/100)*a
      INC count
      IF count>warten
        @dosound("doorbell.au")
        PRINT "Grenze unterschritten!"
        CLR count
        INC alarm
      ENDIF
      IF alarm>5
        @dosound("clock.au")
        PRINT "Aufwachen! Die Grenze ist schon lange unterschritten!"
        CLR alarm
      ENDIF
    ELSE if ueber AND b>(1+grenze/100)*a
      INC ucount
      IF ucount>warten
        @dosound("whistle.au")
        PRINT "Grenze ueberschritten!"
        CLR ucount
        INC ualarm
      ENDIF
      IF ualarm>5
        @dosound("ahhhhh.wav")
        PRINT "Aufwachen! Die Grenze ist schon lange ueberschritten!"
        CLR ualarm
      ENDIF
    ELSE
      CLR count
      CLR ucount
      CLR alarm
      CLR ualarm
    ENDIF
    IF fix=0 AND b>a
      a=b
      IF hssound
        @dosound("spacemusic.au")
      ENDIF
      PRINT "Neuer Highscore!"
    ENDIF
    dipol$=csget$("ELS_MAGNETE_DIPOL.STATUS_SM")
    IF dipol$<>dipolold$ AND flag=0
      PRINT "DIPOL-Error:"
      flag=1
      @dosound("ST_Exclamation.wav")
    ENDIF
    IF dipol$=dipolold$
      flag=0
    ENDIF
    linachf=CSGET("INJ_LINAC2_MOD.MOD_BEREIT_DM")
    IF linachf=0 AND lflag=0
      linacfreigabe=CSGET("RAD_INTERLOCK_LINAC1.FREIGABE_DM")
      IF linacfreigabe=1
        PRINT "Linac 2 HF ausgefallen!"
        @dosound("car_cras.wav")
      ELSE
        PRINT "Linac 2 HF aus, Interlock nicht ok."
      ENDIF
      lflag=1
    ENDIF
    IF linachf=1
      lflag=0
    ENDIF
  ENDIF
  IF inp?(-2)
    eingabe=INP(-2)
    ' print eingabe
    ' Leerzeichen - aktueller Messwert als Maximalwert
    IF eingabe=32
      a=b
      PRINT "Maximalwert auf ";a;" gesetzt."
      fix=0
      ' a - neue maximale Abweichung eingeben
    ELSE if eingabe=97
      INPUT "Neue maximale Abweichung in Prozent", grenze
      IF grenze<0
        grenze=0
      ENDIF
      PRINT "Neue maximale Abweichung ist ";grenze;" %"
      ' f - festen Schwellwert eingeben
    ELSE if eingabe=102
      INPUT "Fester Bezugswert in pC",a
      IF a<0
        a=0
      ENDIF
      PRINT "Fester Bezugswert ist ";a;" pC, kein Unterschreiten erlaubt"
      fix=1
      grenze=0
      ' h - Hilfe
    ELSE if eingabe=104
      GOSUB help
    ELSE if eingabe=111
      PRINT "Alter Offset ist ",offset;" pC"
      INPUT "Neuer Offset in pC",offset
      PRINT "Neuer Offset ist ",offset;" pC"
    ELSE if eingabe=79
      offset=offset+b
      PRINT "Offset wurde gesetzt auf ";offset;" pC"
    ELSE if eingabe=112
      IF pause=0
        PRINT "Ausgabe angehalten, zum Weitermachen <p> druecken."
      ENDIF
      pause=1-pause
    ELSE if eingabe=108
      nosound=1-nosound
      IF nosound=0
        PRINT "Sound ist jetzt an"
      ELSE
        PRINT "Sound ist jetzt aus"
      ENDIF
    ELSE if eingabe=115
      PRINT "Ab jetzt wird sofort gewarnt!"
      warten=0
    ELSE if eingabe=83
      PRINT "Ab jetzt wird wieder spaeter gewarnt."
      warten=wartedelay
    ELSE if eingabe=117
      PRINT "Jetzt wird auch bei Ueberschreiten gewarnt."
      ueber=1
    ELSE if eingabe=85
      PRINT "Jetzt wird nicht mehr bei Ueberschreiten gewarnt."
      ueber=0
    ELSE if eingabe=106
      hssound=1-hssound
      IF hssound=1
        PRINT "Bei Highscore wird jetzt jubiliert."
      ELSE
        PRINT "Bei Highscore wird jetzt nicht mehr jubiliert."
      ENDIF
    ELSE if eingabe=119
      PRINT "Altes Warndelay ist ";wartedelay;" Zyklen"
      INPUT "Neues Warndelay in Zyklen",wartedelay
      IF wartedelay<1
        wartedelay=1
      ENDIF
      PRINT "Neues Warndelay ist ";wartedelay;" Zyklen"
      warten=wartedelay
    ELSE if eingabe=122
      PRINT "Alte Zeit zwischen Updates ist ";sleeptime;" s"
      INPUT "Neue Zeit",sleeptime
      IF sleeptime<1
        sleeptime=1
      ENDIF
    ELSE if eingabe=116
      PRINT "Teste Lautstaerke..."
      @dosound("doorbell.au")
    ELSE if eingabe=27
      QUIT
    ENDIF
  ENDIF
LOOP
QUIT

PROCEDURE help
  PRINT
  PRINT "Bedienung:"
  PRINT "<space> Bezugswert auf aktuellen Wert setzen"
  PRINT "<a> Neue maximale Abweichung eingeben, momentan ";grenze;" %"
  PRINT "<f> Festen Grenzwert eingeben, Abweichung auf Null"
  PRINT "<o> Neuen Offset eingeben (momentan ";offset;" pC)"
  PRINT "<O> Momentanen Wert als Offset abziehen"
  PRINT "<s> Sofort bei Abweichung warnen"
  PRINT "<S> Spaeter (mit Warndelay von ";wartedelay;" Zyklen) warnen"
  PRINT "<w> Anderes Warndelay setzen, momentan ";wartedelay;" Zyklen"
  PRINT "<u> Auch bei Ueberschreiten warnen"
  PRINT "<U> Nicht mehr bei Ueberschreiten warnen"
  PRINT "<j> Bei neuem Highscore jubilieren"
  PRINT "<p> Pause (bei Strahlunterbrechungen)"
  PRINT "<z> Setze Zeit zwischen Updates (momentan ";sleeptime;" s)"
  PRINT "<t> Lautstaerke testen"
  PRINT "<l> Laut/leise - Sound an und aus schalten";
  IF nosound
    PRINT " (ist aus)"
  ELSE
    PRINT " (ist an)"
  ENDIF
  PRINT "<h> Diese Hilfe ausgeben"
  PRINT "<esc> Beenden"
  PRINT
  PRINT "Sollwert [pC]   Aktueller Wert [pC]"
RETURN

PROCEDURE dosound(sndfile$)
  IF nosound=0
    SYSTEM "send_sound -server elsahp2 /sgt/ccs/snd/"+sndfile$
  ENDIF
RETURN

FUNCTION fstr$(z,i)
  LOCAL s$
  IF z<0
    s$="-"
  ELSE
    s$=" "
  ENDIF
  s$=s$+STR$(abs(INT(z)),5)+"."+STR$(abs(INT(frac(z)*10^i)),i,,1)
  RETURN s$
ENDFUNCTION
