
' Funktion in X11-Basic (c) Markus Hoffmann 2005-06-22
' Die Formel gab es irgendwo im Internet.
' Wer verstehts? Aber es funktioniert.

WHILE a<JULIAN(date$)
  a=@mondtag(nn,2)
  INC nn
  PRINT nn,JULDATE$(ROUND(a))
WEND
QUIT

' nph=0  Neumond
' nph=1  1. viertel
' nph=2  Vollmond
' nph=3  letztes viertel

' Rueckgabe ist julianischer tag

FUNCTION mondtag(n,nph)
  LOCAL c,t,t2,as,am,jd
  c=n+nph/4
  t=c/1236.85
  as=359.2242+29.105356*c+((1.178e-4)-(1.55e-7)*t)*t^2
  am=306.0253+385.816918*c+0.010730*t^2
  jd=2415020+28*n+7*nph+0.75933+1.53058868*c
  IF nph=0 OR nph=2
    ADD jd,(0.1734-3.93e-4*t)*SIN(RAD(as))-0.4068*SIN(RAD(am))
  ELSE IF nph=1 OR nph=3
    ADD jd,(0.1721-4e-4*t)*SIN(RAD(as))-0.6280*SIN(RAD(am))
  ELSE
    RETURN 0
  ENDIF
  RETURN jd
ENDFUNCTION
