' Traveling salesman problem
' This program generates a graph and trys to find a shortest route
' through it.
' written in X11-Basic                 (c) Markus Hoffmann 2008
'

anzpoints=400

bw=400

DIM x(anzpoints+1),y(anzpoints+1)
DIM path(anzpoints+1)
DIM ou(anzpoints+1)

PRINT "Init -1-"

ARRAYFILL ou(),0

FOR i=0 TO anzpoints-1
  ' x(i)=random(bw)
  ' y(i)=random(bw)
  x(i)=(i MOD 20)*bw/20+RANDOM(5)
  y(i)=(i DIV 20)*bw/20+RANDOM(5)
  ' x(i)=(i mod 10)*bw/10
  ' y(i)=(i div 10)*bw/10
NEXT i
PRINT "Init -2-"

c=0
ou(0)=1
FOR i=0 TO anzpoints-1
  nn=@neighbour(c)
  path(c)=nn
  ou(nn)=1
  c=nn
NEXT i
path(c)=0

PRINT "Go --"

weiss=COLOR_RGB(1,1,1)
schwarz=COLOR_RGB(0,0,0)

@plotit

PRINT @length

mindist=dist
adist=dist

DO
  FOR aa=0 TO anzpoints-1
    FOR bb=0 TO aa-1
      ' aa=random(anzpoints)
      ' bb=random(anzpoints)
      @perm(aa,bb)
      @plotit
      IF dist<mindist
        mindist=dist
      ELSE
        @perm(aa,bb)
      ENDIF
      PLOT 400+zeit,(adist-mindist)
      INC zeit
      IF zeit>240
        zeit=0
      ENDIF
    NEXT bb
  NEXT aa
LOOP
QUIT

PROCEDURE perm(a,b)
  tx=x(aa)
  ty=y(aa)
  x(aa)=x(bb)
  y(aa)=y(bb)
  x(bb)=tx
  y(bb)=ty
RETURN

' Plot the nodes and the actual path
'
PROCEDURE plotit
  COLOR schwarz
  PBOX 0,0,bw,bw
  COLOR weiss
  CLR dist

  FOR i=0 TO anzpoints-1
    ' text x(i),y(i),str$(i)
    PCIRCLE x(i),y(i),2
  NEXT i
  cc=0
  FOR i=0 TO anzpoints-1
    ' print cc;"-->";
    LINE x(cc),y(cc),x(path(cc)),y(path(cc))
    ADD dist,@dist(cc,path(cc))
    cc=path(cc)
  NEXT i
  LINE x(path(cc)),y(path(cc)),x(0),y(0)
  ADD dist,@dist(0,path(cc))
  SHOWPAGE
  PRINT "Distance: ";dist
RETURN

FUNCTION neighbour(a)
  LOCAL i,b,n
  ' print "A:";a
  n=a
  gg=2*256
  FOR i=0 TO anzpoints-1
    IF ou(i)=0 AND NOT a=i
      b=@dist(a,i)
      IF b<gg
        n=i
        gg=b
        ' print n,gg
      ENDIF
    ENDIF
  NEXT i
  RETURN n
ENDFUNCTION

DEFFN dist(ii,jj)=SQRT((x(ii)-x(jj))^2+(y(ii)-y(jj))^2)

FUNCTION length()
  LOCAL i,l,cc
  cc=0
  l=0
  FOR i=0 TO anzpoints-1
    ADD l,@dist(cc,path(cc))
    cc=path(cc)
  NEXT i
  ADD l,@dist(path(cc),0)
  RETURN l
ENDFUNCTION

