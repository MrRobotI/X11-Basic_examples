' Calculates the inverse of a number in Zn
'
' X11-Basic by Markus Hoffmann 2014-04-04
'
'
n=256001
PRINT "Calculates the inverse of a number in Zn ; n=";n
FOR x=0 TO n-1 STEP 1
  t=CTIMER
  PRINT x;" --> ";@inv1(x,n),
  FLUSH
  t2=CTIMER
  PRINT " ";@inv2(x,n),
  PRINT INT((t2-t)*100),INT((CTIMER-t2)*100)
NEXT x
QUIT
FUNCTION inv1(x,n)
  LOCAL i
  FOR i=0 TO n-1
    IF ((i*x) MOD n)=1
      RETURN i
    ENDIF
  NEXT i
  RETURN 0
ENDFUNCTION
FUNCTION inv2(x,n)
  LOCAL i,dd,ss,tt
  CLR dd,ss,tt
  @extended_euclid(x,n,dd,ss,tt)
  IF dd=1
    ss=ss MOD n
    IF ss<0
      ADD ss,n
    ENDIF
    RETURN ss
  ELSE
    RETURN 0
  ENDIF
ENDFUNCTION

PROCEDURE extended_euclid(a,b, VAR d, VAR s, VAR t)
  LOCAL d2,s2,t2,a2,b2
  ' print "(";a;",";b;")";
  IF b=0
    d=a
    s=1
    t=0
  ELSE
    a2=b
    b2=a MOD b
    @extended_euclid(a2,b2,d2,s2,t2)
    d=d2
    s=t2
    t=s2-FLOOR(a/b)*t2
  ENDIF
RETURN
