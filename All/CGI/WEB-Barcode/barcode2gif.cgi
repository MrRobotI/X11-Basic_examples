#!/usr/bin/xbasic
'
' barcode2gif.cgi (c) Markus Hoffmann 2011   V. 1.01
' darf mit dem Paket X11-Basic weitergegeben werden
'
tmp$="/tmp/barcode-"+str$(timer)+"."
comment$="Generated using iec16022/linux with barcode2gif.cgi (c) Markus Hoffmann"
t$=env$("REQUEST_URI")
SPLIT t$,"?",1,a$,t$
size=3
IF len(t$)<2
  code$="XXX-XXX-00000-000.000"
  size=3
ELSE
  SPLIT t$,"&",1,a$,t$
  WHILE len(a$)
    SPLIT a$,"=",1,a$,b$
    IF a$="code"
      code$=@purify$(b$)
    ELSE if a$="size"
      size=val(b$)
    ELSE if a$="fix"
      fix=val(b$)
    ELSE if a$="x"
      x=val(b$)
    ELSE if a$="y"
      y=val(b$)
    ENDIF
    SPLIT t$,"&",1,a$,t$
  WEND
ENDIF
IF fix
  SYSTEM "echo 'Content-type: image/gif';echo ; /usr/bin/iec16022 -s "+str$(x)+"x"+str$(y)+" -f PNG -o "+tmp$+".png "+chr$(34)+code$+chr$(34)+"; convert -comment "+chr$(34)+comment$+chr$(34)+" -scale "+str$(size*100)+"% "+tmp$+".png "+tmp$+".gif ; cat "+tmp$+".gif"
ELSE
  SYSTEM "echo 'Content-type: image/gif';echo ; /usr/bin/iec16022  -f PNG -o "+tmp$+".png "+chr$(34)+code$+chr$(34)+"; convert -comment "+chr$(34)+comment$+chr$(34)+" -scale "+str$(size*100)+"% "+tmp$+".png "+tmp$+".gif ; cat "+tmp$+".gif"
ENDIF
SYSTEM "rm -f "+tmp$+".png "+tmp$+".gif "
QUIT

FUNCTION purify$(g$)
  LOCAL i
  g$=replace$(g$,"+"," ")
  g$=replace$(g$,"%0A"," ")
  g$=replace$(g$,"%0D"," ")
  FOR i=0 TO 255
    g$=replace$(g$,"%"+upper$(hex$(i,2,2)),chr$(i))
  NEXT i
  RETURN g$
ENDFUNCTION
