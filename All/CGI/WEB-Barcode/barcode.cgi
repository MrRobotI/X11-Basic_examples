#!/usr/bin/xbasic
host$=""
'
' barcode.cgi (c) Markus Hoffmann 2011   V. 1.01
' darf mit dem Paket X11-Basic weitergegeben werden
'

PRINT "Content-type: text/html"
PRINT
size=3
dmfixed=0
dmx=20
dmy=20
code$="XXX-XXX-00000-000.000"
rawcode$=code$
t$=env$("REQUEST_URI")
SPLIT t$,"?",1,a$,t$
SPLIT t$,"?",1,t$,u$

SPLIT t$,"&",1,a$,t$
WHILE len(a$)
  SPLIT a$,"=",1,a$,b$
  IF a$="code"
    rawcode$=b$
    code$=@purify$(b$)
  ELSE if a$="size"
    size=val(b$)
  ELSE if a$="dmfixed"
    dmfixed=val(b$)
  ELSE if a$="dmx"
    dmx=val(b$)
  ELSE if a$="dmy"
    dmy=val(b$)
  ENDIF
  SPLIT t$,"&",1,a$,t$
WEND

r$=env$("REMOTE_ADDR")
h$=env$("REMOTE_HOST")
IF not exist("/tmp/WEBBARCODE.log")
  OPEN "O",#1,"/tmp/WEBBARCODE.log"
ELSE
  OPEN "A",#1,"/tmp/WEBBARCODE.log"
ENDIF
PRINT #1,date$+" "+time$+" "+r$+" "+h$+" "+code$
CLOSE #1

gifname$="barcodegif.cgi?code="+rawcode$+"&size="+str$(size)
gif2name$="barcode2gif.cgi?code="+rawcode$+"&size="+str$(size)

IF dmfixed
  gif2name$=gif2name$+"&fix="+str$(dmfixed)+"&x="+str$(dmx)+"&y="+str$(dmy)
ENDIF

PRINT "<HTML> <HEAD> <TITLE>Markus Hoffmann's Barcodegenerator</TITLE></HEAD>"
PRINT "<BODY bgcolor="#ffffff" link=2200aa vlink=008800>"
PRINT "<h6>(c) Markus Hoffmann 2011"
VERSION
PRINT "</h6>"
PRINT "<center><H1> Barcodegenerator</H1>"
PRINT "Geben Sie den zu Kodierenden Text ein. Mit Size bestimmen Sie die Gr&ouml;&szlig;e des Labels."
PRINT "Klicken Sie den Button 'Barcode erzeugen'. Daraufhin werden Barcodes im QR-Code sowie im Datamatrix"
PRINT "Code erzeugt. <p> Diese Labels koennen Sie als .GIF Dateien speichern. (Mit der rechten Maustaste"
PRINT "draufklicken).<p> Mit verschiedenen Anwendungen, k&ouml;nnen Sie das Label wieder in text zurueckver";
PRINT "wandeln. Z.B. mit einer IPhone App, mit Zbar (linux) oder auch Zbar (WINDOWS)."
PRINT "<HR>"
PRINT "<form name=querybox action="+host$+"/cgi-bin/barcode.cgi method=get>"
PRINT "<table border=1 cellspacing=0><tr><td>"
PRINT "<img src=/cgi-bin/"+gifname$+"><br><a href='http://de.wikipedia.org/wiki/QR-Code'>QR-Code</a><td>"
PRINT "<img src=/cgi-bin/"+gif2name$+"><br> <a href='http://de.wikipedia.org/wiki/DataMatrix'>DataMatrix</a><br>"
PRINT "<input type=text name=dmx value="+str$(dmx)+" size=3> x <input type=text name=dmy value="+str$(dmy)+" size=3>"
PRINT "<input type=checkbox name=dmfixed value=1 ";
IF dmfixed
  PRINT "checked=checked"
ENDIF
PRINT "> Fixed Size<br><td>"

PRINT "Code=<input type=text name=code value="+chr$(34)+code$+chr$(34)+" size=40><br>"
PRINT "size=<input type=text name=size value="+str$(size)+" size=8><br>"
PRINT ""
PRINT "<input value="+chr$(34)+"Barcode erzeugen"+chr$(34)+" type="+chr$(34)+"submit"+chr$(34)+">"
PRINT "<p>"
PRINT "</table>"
PRINT "</form><p></center><HR>"
PRINT "<I>Kommentare oder Anregungen zu dieser WWW-Seite bitte "
PRINT "<A HREF=mailto:markus.hoffmann@desy.de>hierhin</A>.</I><P>"
PRINT "<FONT FACE="+chr$(34)+"ARIAL,HELVETICA"+chr$(34)+" SIZE=1>"
PRINT "Erzeugt am "+time$+" "+date$
PRINT "</FONT></BODY></HTML>"
QUIT

FUNCTION purify$(g$)
  LOCAL i
  g$=replace$(g$,"+"," ")
  g$=replace$(g$,"%0A"," ")
  g$=replace$(g$,"%0D"," ")
  FOR i=0 TO 255
    g$=replace$(g$,"%"+upper$(hex$(i,2,2)),chr$(i))
  NEXT i
  RETURN g$
ENDFUNCTION
