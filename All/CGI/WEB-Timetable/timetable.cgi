#!/usr/bin/xbasic
' Produce a pretty timetable from the Employees schedule data
' cgi-script with X11-Basic (c) Markus Hoffmann
PRINT "Content-type: text/html"+chr$(13)
PRINT ""+chr$(13)
FLUSH
PRINT "<html><head><TITLE>Timetable CGI</TITLE><head><body>"
PRINT "<pre>"
version$="1.00"

@init

ndays=90

anzpeople=200

DIM table$(ndays,anzpeople)
DIM name$(anzpeople)
DIM timestamp(ndays)

DIM awaystart(20),awayend(20),awaylocation$(20),awayreason$(20)

ARRAYFILL table$(),"&nbsp;"
ARRAYFILL name$(),"Name"

today=julian(date$)

FOR i=0 TO ndays-1
  timestamp(i)=today-7+i
NEXT i

anzpeople=0

' Namen prozessieren

content$=@getpagecontent("Employees_Schedule")
content$=replace$(content$,"||",chr$(10)+"|")

' print content$

flag2=0
WHILE len(content$)
  SPLIT content$,chr$(10),0,t$,content$
  IF glob(t$,"{|*")
    flag2=1
  ENDIF
  IF flag2
    @processline(t$)
    IF glob(t$,"*|}*")
      flag2=0
      BREAK
    ENDIF
  ENDIF
WEND
PRINT "</pre>"

' print "<h1>Timetable of next "+str$(ndays)+" days</h1>"

PRINT "<table border=2 width=1000% cellspacing=0>"

' 1. Header-Zeile (Wochentag)
PRINT "<tr><td></td>"
FOR i=0 TO ndays-1
  status1=@isfeiertag(timestamp(i))
  vollmond1=@isvollmond(timestamp(i))

  IF timestamp(i)=today
    PRINT "<th bgcolor=00ffff>"
  ELSE if (timestamp(i) mod 7)=5 OR status1=2
    PRINT "<th bgcolor=ffff00>"
  ELSE if (timestamp(i) mod 7)=6 OR status1=1
    PRINT "<th bgcolor=ff0000>"
  ELSE
    PRINT "<th>"
  ENDIF

  PRINT wochentage$(timestamp(i)+1 mod 7)

  IF vollmond1
    PRINT " O "
  ENDIF
  PRINT "</th>"
NEXT i
PRINT "</tr>"
' 2. Header-Zeile (Tag im Monat)
PRINT "<tr><td></td>"
FOR i=0 TO ndays-1
  status1=@isfeiertag(timestamp(i))
  vollmond1=@isvollmond(timestamp(i))

  IF timestamp(i)=today
    PRINT "<th bgcolor=00ffff>"
  ELSE if (timestamp(i) mod 7)=5 OR status1=2
    PRINT "<th bgcolor=ffff00>"
  ELSE if (timestamp(i) mod 7)=6 OR status1=1
    PRINT "<th bgcolor=ff0000>"
  ELSE
    PRINT "<th>"
  ENDIF
  PRINT left$(juldate$(timestamp(i)),5)
  PRINT "</th>"
NEXT i
PRINT "</tr>"
PRINT "<tr><td></td>"
FOR i=0 TO ndays-1
  status1=@isfeiertag(timestamp(i))
  IF timestamp(i)=today
    PRINT "<th bgcolor=00ffff>"
  ELSE if (timestamp(i) mod 7)=5 OR status1=2
    PRINT "<th bgcolor=ffff00>"
  ELSE if (timestamp(i) mod 7)=6 OR status1=1
    PRINT "<th bgcolor=ff0000>"
  ELSE
    PRINT "<th>"
  ENDIF
  IF status1
    PRINT feier1$
  ENDIF
  PRINT "</th>"
NEXT i
PRINT "</tr>"

FOR i=0 TO anzpeople-1
  PRINT "<tr>"
  IF odd(i)
    PRINT "<th bgcolor=dddddd>";
  ELSE
    PRINT "<th>";
  ENDIF
  PRINT name$(i)+"</th>"
  FOR j=0 TO ndays-1
    status1=@isfeiertag(timestamp(j))
    IF timestamp(j)=today
      PRINT "<td bgcolor=00ffff>"
    ELSE if (timestamp(j) mod 7)=5 OR status1=2
      PRINT "<td bgcolor=ffffaa>"
    ELSE if (timestamp(j) mod 7)=6 OR status1=1
      PRINT "<td bgcolor=ffaaaa>"
    ELSE if odd(i)
      PRINT "<td bgcolor=dddddd>"
    ELSE
      PRINT "<td>"
    ENDIF
    PRINT table$(j,i)
    PRINT "</td>"
  NEXT j
  PRINT "</tr>"
NEXT i
PRINT "</table>"
PRINT "<hr><h6>(c) Markus Hoffmann timetable.cgi V."+version$+" with <a href="+chr$(34)+"http://x11-basic.sourceforge.net/"+chr$(34)+">X11-Basic</a></h6></body></html>"
FLUSH
QUIT

PROCEDURE init
  DIM wochentage$(7)
  wochentage$(0)="Sun"
  wochentage$(1)="Mon"
  wochentage$(2)="Tue"
  wochentage$(3)="Wed"
  wochentage$(4)="Thu"
  wochentage$(5)="Fri"
  wochentage$(6)="Sat"
  wochentage$(7)="Sun"
  DIM monat$(12)
  monat$(1)="Januar"
  monat$(2)="Februar"
  monat$(3)="M\344rz"
  monat$(4)="April"
  monat$(5)="Mai"
  monat$(6)="Juni"
  monat$(7)="Juli"
  monat$(8)="August"
  monat$(9)="September"
  monat$(10)="Oktober"
  monat$(11)="November"
  monat$(12)="Dezember"
  jahr=val(right$(date$,4))

  DIM feierp$(100),feiern$(100),feiers(100)
  anzfeiertage=0
  DO
    READ a$
    EXIT if a$="***"
    feierp$(anzfeiertage)=a$
    READ b$
    READ c
    feiern$(anzfeiertage)=b$
    feiers(anzfeiertage)=c
    INC anzfeiertage
  LOOP
  @ostern(jahr)
RETURN

' Feiertage
DATA "01.01.*","Neujahr",1
DATA "06.01.*","Heilige 3 K�nige",3
DATA "20.03.*","Fr�hlingsanfang",3
DATA "01.05.*","Mai-Feiertag",1
DATA "05.06.2007","XFEL start",3
DATA "09.06.2007","Nacht des Wissens",3
DATA "29.06.2007","HERA ex party",3
DATA "21.06.*","Sommeranfang",3
DATA "15.08.*","Mari� Himmelfahrt",3
DATA "03.10.*","Tag der Deutschen Einheit",1
DATA "31.10.*","Reformationstag",3
DATA "01.11.*","Allerheiligen",3
DATA "24.12.*","Heiligabend",2
DATA "25.12.*","1. Weihnachtstag",1
DATA "26.12.*","2. Weihnachtstag",1
DATA "31.12.*","Silvester",2
DATA "***"
PROCEDURE ostern(jahr)
  q=jahr div 4
  a=jahr mod 19
  b=(204-11*a) mod 30
  IF b=28 OR b=28
    DEC b
  ENDIF
  i=b
  j=jahr+q+i-13
  c=j-(j DIV 7)*7
  o=28+i-c
  IF o<=31
    dd$=str$(o,2,2,1)+".03."+str$(jahr,4,4,1)
  ELSE
    dd$=str$(o-31,2,2,1)+".04."+str$(jahr,4,4,1)
  ENDIF
  feierp$(anzfeiertage)=dd$
  feiern$(anzfeiertage)="Ostersonntag"
  feiers(anzfeiertage)=1
  INC anzfeiertage
  feierp$(anzfeiertage)=juldate$(julian(dd$)+1)
  feiern$(anzfeiertage)="Ostermontag"
  feiers(anzfeiertage)=1
  INC anzfeiertage
  feierp$(anzfeiertage)=juldate$(julian(dd$)-2)
  feiern$(anzfeiertage)="Karfreitag"
  feiers(anzfeiertage)=1
  INC anzfeiertage
  feierp$(anzfeiertage)=juldate$(julian(dd$)-3)
  feiern$(anzfeiertage)="Gr�ndonnerstag"
  feiers(anzfeiertage)=3
  INC anzfeiertage
  feierp$(anzfeiertage)=juldate$(julian(dd$)-46)
  feiern$(anzfeiertage)="Aschermittwoch"
  feiers(anzfeiertage)=3
  INC anzfeiertage
  feierp$(anzfeiertage)=juldate$(julian(dd$)-47)
  feiern$(anzfeiertage)="Fastnacht"
  feiers(anzfeiertage)=3
  INC anzfeiertage
  feierp$(anzfeiertage)=juldate$(julian(dd$)-48)
  feiern$(anzfeiertage)="Rosenmontag"
  feiers(anzfeiertage)=3
  INC anzfeiertage
  feierp$(anzfeiertage)=juldate$(julian(dd$)-52)
  feiern$(anzfeiertage)="Weiberfastnacht"
  feiers(anzfeiertage)=3
  INC anzfeiertage
  feierp$(anzfeiertage)=juldate$(julian(dd$)+39)
  feiern$(anzfeiertage)="Christi Himmelfahrt"
  feiers(anzfeiertage)=1
  INC anzfeiertage
  feierp$(anzfeiertage)=juldate$(julian(dd$)+49)
  feiern$(anzfeiertage)="Pfingstsonntag"
  feiers(anzfeiertage)=1
  INC anzfeiertage
  feierp$(anzfeiertage)=juldate$(julian(dd$)+50)
  feiern$(anzfeiertage)="Pfingstmontag"
  feiers(anzfeiertage)=1
  INC anzfeiertage
  feierp$(anzfeiertage)=juldate$(julian(dd$)+60)
  feiern$(anzfeiertage)="Fronleichnam"
  feiers(anzfeiertage)=3
  INC anzfeiertage
RETURN
' nph=0  Neumond
' nph=1  1. viertel
' nph=2  Vollmond
' nph=3  letztes viertel

' Rueckgabe ist julianischer tag

FUNCTION mondtag(n,nph)
  LOCAL c,t,t2,as,am,jd
  c=n+nph/4
  t=c/1236.85
  as=359.2242+29.105356*c+((1.178e-4)-(1.55e-7)*t)*t^2
  am=306.0253+385.816918*c+0.010730*t^2
  jd=2415020+28*n+7*nph+0.75933+1.53058868*c
  IF nph=0 OR nph=2
    ADD jd,(0.1734-3.93e-4*t)*sin(rad(as))-0.4068*sin(rad(am))
  ELSE if nph=1 OR nph=3
    ADD jd,(0.1721-4e-4*t)*sin(rad(as))-0.6280*sin(rad(am))
  ELSE
    RETURN 0
  ENDIF
  RETURN jd
ENDFUNCTION
FUNCTION isfeiertag(jd)
  LOCAL j
  status1=0
  FOR j=0 TO anzfeiertage-1
    IF glob(juldate$(jd),feierp$(j))
      feier1$=feiern$(j)
      status1=feiers(j)
    ENDIF
  NEXT j
  RETURN status1
ENDFUNCTION
FUNCTION isvollmond(jd)
  LOCAL a,nn
  vollmond1=0
  ' Vollmond finden
  a=0
  nn=(jahr-1900)*12
  WHILE a<jd+14
    a=@mondtag(nn,2)
    INC nn
    ' print nn,juldate$(round(a))
    IF round(a)=jd+i-1
      vollmond1=true
    ENDIF
  WEND
  RETURN vollmond1
ENDFUNCTION

PROCEDURE processline(ln$)
  IF left$(ln$,2)="|}"
  ELSE if left$(ln$,2)="{|"
  ELSE if left$(ln$,9)="|colspan="
  ELSE if left$(ln$,9)="|rowspan="
  ELSE if left$(ln$,7)="|align="
  ELSE if left$(ln$,4)="|'''"
  ELSE
    IF left$(ln$)="|"
      IF left$(ln$,2)="|-"
        IF len(name$(anzpeople))
          IF awaycount>0
            FOR kk=0 TO awaycount-1
              FOR ii=0 TO ndays-1
                IF timestamp(ii)>=awaystart(kk) AND timestamp(ii)<=awayend(kk)
                  IF len(awaylocation$(kk))
                    table$(ii,anzpeople)=table$(ii,anzpeople)+awaylocation$(kk)+"<br>"+awayreason$(kk)
                  ELSE
                    table$(ii,anzpeople)=table$(ii,anzpeople)+"*"+"<br>"+awayreason$(kk)
                  ENDIF
                ENDIF
              NEXT ii
            NEXT kk
          ENDIF
          INC anzpeople
        ENDIF
        awaycount=0
        ARRAYFILL awaylocation$(),""
        ARRAYFILL awayreason$(),""
        ARRAYFILL awaystart(),0
        ARRAYFILL awayend(),0
        c=0
      ELSE
        ln$=right$(ln$,len(ln$)-1)
        a$=ln$
        a$=replace$(a$,"]","")
        a$=replace$(a$,"[","")
        a$=replace$(a$,"&lt;","<")
        a$=replace$(a$,"&gt;",">")
        IF c=0
          SPLIT a$,"|",0,a$,b$
          IF len(b$)
            a$=b$
          ENDIF
          name$(anzpeople)=a$
        ELSE if c=1
          name$(anzpeople)=a$+" "+name$(anzpeople)
        ELSE if c=2
          SPLIT a$,"<br>",0,a$,b$
          SPLIT a$,"|",0,a$,b$
          IF len(b$)
            a$=b$
          ENDIF
          name$(anzpeople)="<a href="+a$+"> "+name$(anzpeople)+"</a>"
        ELSE if c=3
        ELSE if c=4
        ELSE if c=5
          awaycount=0
          WHILE len(a$)
            SPLIT a$,"<br>",0,b$,a$
            awaystart(awaycount)=julian(b$)
            INC awaycount
          WEND
        ELSE if c=6
          awaycount=0
          WHILE len(a$)
            SPLIT a$,"<br>",0,b$,a$
            awayend(awaycount)=julian(b$)
            INC awaycount
          WEND
        ELSE if c=7
          FOR mmm=0 TO awaycount-1
            SPLIT a$,"<br>",0,b$,a$
            awaylocation$(mmm)=b$
          NEXT mmm
        ELSE if c=8
          FOR mmm=0 TO awaycount-1
            SPLIT a$,"<br>",0,b$,a$
            awayreason$(mmm)=b$
          NEXT mmm
        ELSE
          PRINT a$,c
        ENDIF
        INC c
      ENDIF
    ENDIF
  ENDIF
RETURN

FUNCTION getpagecontent(p$)
  LOCAL page$
  LOCAL server$
  LOCAL a$,b$,t$
  ret$=""
  server$="mskpc14.desy.de"
  page$="/wiki/index.php?title="+p$+"&action=raw&"
  OPEN "UC",#1,server$,80
  PRINT #1,"GET "+page$+" HTTP/1.0"+chr$(13)
  PRINT #1,"Host: "+server$+chr$(13)
  PRINT #1,"User-Agent: X11-Basic/1.12"+chr$(13)
  PRINT #1,chr$(13)
  FLUSH #1
  LINEINPUT #1,response$
  SPLIT response$," ",0,protocol$,response$
  SPLIT response$," ",0,htmlerror$,response$
  '  print "Response: ";response$
  IF val(htmlerror$)=200
    ' Parse Header
    LINEINPUT #1,t$
    t$=replace$(t$,chr$(13),"")
    WHILE len(t$)
      ' print t$,len(t$)
      SPLIT t$,":",0,a$,b$
      IF a$="Content-Length"
        length=val(b$)
      ENDIF
      LINEINPUT #1,t$
      t$=replace$(t$,chr$(13),"")
    WEND
    ' print "Len=";length;" Bytes."
    IF length
      ret$=input$(#1,length)
    ENDIF
  ELSE
    PRINT "Error: could not get data from the WIKI!"
  ENDIF
  CLOSE #1
  RETURN replace$(ret$,chr$(13),"")
ENDFUNCTION
