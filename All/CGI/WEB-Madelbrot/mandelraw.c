
/******************************************************************/
/*                                                                */
/* Routine fuer die 2-D Mandelbrot-Grafikausgabe                  */
/*                                                                */
/* little program to produce Mandelbrod fractals                  */
/* output is a raw image (256 colors) default size: 256x256       */
/*                                                                */
/* to produce a gif-image:                                        */
/* mandelraw | raw2gif -p colormap -s 256 256 > my.gif            */
/*                                                                */
/* compile: gcc mandelraw.c -o mandelraw                          */
/*                                                                */
/*                                                                */
/* (c) Markus Hoffmann 1995        Letzte Bearbeitung: 30.03.1996 */
/*                                                                */
/******************************************************************/

/* INCLUDEs   */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define val(a)   atof(a)

/*********************************************************************/
/* GLOBALe VARIABLEn                                                  */
/*********************************************************************/

double ymin,ymax,xmin,xmax;

/* Arbeitsbereich (Koordinatenbox) */
int bx=0,by=0,bw=256,bh=256;

/*  DEFAULT Werte */

double xxmin=-0.75;
double xxmax=-.722;
double yymin=0.155;
double yymax=0.18;

int kx(double x) {return((x-xmin)*bw/(xmax-xmin)+bx);}
int ky(double y) {return(-bh/(ymax-ymin)*(y-ymax)+by);}
double ox(int x) {return((x-bx)*(xmax-xmin)/bw+xmin);}
double oy(int y) {return(-(y-by)*(ymax-ymin)/bh+ymax);}

typedef struct { double re; double im; } complex; 

void doredraw() {
  int i,j,k,l,o,x,y; 
  double cz1,ii,jj;
  complex z;
  xmin=xxmin;  ymin=yymin;  xmax=xxmax;  ymax=yymax;

  x=bx;y=by;
  k=by+bh;
  l=bx+bw;
  for(j=y;j<k;j++) { 
    for(i=x;i<l;i++) {
      z.re=0; z.im=0;   ii=ox(i); jj=oy(j);
      for(o=0;o<256;o++) {
	cz1=z.re*z.re-z.im*z.im+ii;
	z.im=2*z.re*z.im+jj;
	z.re=cz1;
	if((z.re*z.re+z.im*z.im)>16) break;
      }
      printf("%c",o);
    }
  }
} 

void usage(){
  fprintf(stderr,"\n Usage:\n ------ \n");
  fprintf(stderr," mandelraw <x1> <x2> <y1> <y2> [<bw> <bh>]\n\n");
  fprintf(stderr,"   x1  -- Koordinate\n");
  fprintf(stderr,"   x2  -- Koordinate\n");
  fprintf(stderr,"   y1  -- Koordinate\n");
  fprintf(stderr,"   y2  -- Koordinate\n");
  fprintf(stderr,"   bw  -- image width in pixels\n");
  fprintf(stderr,"   bw  -- image height in pixels\n");
  fprintf(stderr,"\n");
}

int main (int argc, char *argv[]) { 
  if(argc>=5) {
    xxmin=val(argv[1]);
    xxmax=val(argv[2]);
    yymin=val(argv[3]);
    yymax=val(argv[4]);
  } else usage();
  if(argc>=7) {
    bw=val(argv[5]);
    bh=val(argv[6]);
  }
  doredraw() ;
  return(0);
}  
