#!/usr/bin/xbasic
'
' mandelgif.cgi (c) Markus Hoffmann 1999   V. 1.01
' darf mit dem Paket X11-Basic weitergegeben werden
'
t$=env$("REQUEST_URI")
SPLIT t$,"?",1,a$,t$
IF len(t$)<2
  x1=-2
  y1=-2
  x2=2
  y2=2
ELSE
  SPLIT t$,"&",1,a$,t$
  WHILE len(a$)
    SPLIT a$,"=",1,a$,b$
    IF a$="x1"
      x1=val(b$)
    ELSE if a$="x2"
      x2=val(b$)
    ELSE if a$="y1"
      y1=val(b$)
    ELSE if a$="y2"
      y2=val(b$)
    ENDIF
    SPLIT t$,"&",1,a$,t$
  WEND
ENDIF
SYSTEM "echo 'Content-type: image/gif';echo ; /usr/local/bin/mandelraw "+str$(x1)+" "+str$(x2)+" "+str$(y1)+" "+str$(y2)+" | raw2gif -p /srv/www/cgi-bin/colormap -s 256 256 "
QUIT
