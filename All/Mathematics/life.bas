' LIFE Simulation (c) Markus Hoffmann 2002

weiss=COLOR_RGB(1,1,1)
rot=GET_COLOR(65535,40000,20000)
schwarz=COLOR_RGB(0,0,0)
SHOWPAGE
PAUSE 0.3
GET_GEOMETRY 1,bx%,by%,bw%,bh%
IF bw%=0
  bw%=24
  bh%=24
ELSE
  bw%=bw%/8
  bh%=bh%/8
ENDIF
SIZEW ,bw%*8,bh%*8
DIM f(bw%+1,bh%+1),fold(bw%+1,bh%+1)
ARRAYFILL f(),0

COLOR schwarz
PBOX 0,0,bw%*8,bh%*8
COLOR rot
TEXT 30,10,"life (c) Markus Hoffmann"
@init
pausp=0.01
count%=0
t=timer
DO
  @update
  @evolution
  MOUSE x,y,k
  IF k
    col=1-f(x/8,y/8)
    WHILE k
      IF x>=0 AND y>=0 AND x<bw%*8 AND y<bh%*8
        f(x/8,y/8)=col
        @update
      ENDIF
      MOUSE x,y,k
    WEND
  ENDIF
  INC count%
  IF count%>=10
    COLOR rot
    frames=count%/(timer-t)
    TEXT 8,bh%*8-16,using$(frames," ####.# frames per second. ")
    IF frames>20
      pausp=pausp*1.1
    ELSE
      pausp=pausp*0.9
    ENDIF
    count%=0
    t=timer
  ENDIF
LOOP

PROCEDURE evolution
  LOCAL i%,j%,n%
  fold()=f()
  FOR i%=0 TO bw%-1
    FOR j%=0 TO bh%-1
      n%=0
      IF i%>0
        ADD n%,fold(i%-1,j%)
      ENDIF
      IF i%<bw%-1
        ADD n%,fold(i%+1,j%)
      ENDIF
      IF j%>0
        ADD n%,fold(i%,j%-1)
        IF i%<bw%-1
          ADD n%,fold(i%+1,j%-1)
        ENDIF
        IF i%>0
          ADD n%,fold(i%-1,j%-1)
        ENDIF
      ENDIF
      IF j%<bh%-1
        ADD n%,fold(i%,j%+1)
        IF i%<bw%-1
          ADD n%,fold(i%+1,j%+1)
        ENDIF
        IF i%>0
          ADD n%,fold(i%-1,j%+1)
        ENDIF
      ENDIF
      f(i%,j%)=abs(n%=3 OR (n%=2 AND fold(i%,j%)=1))
    NEXT j%
  NEXT i%
RETURN

PROCEDURE update
  LOCAL i%,j%
  FOR i%=0 TO bw%-1
    FOR j%=0 TO bh%-1
      IF f(i%,j%)<>fold(i%,j%)
        IF f(i%,j%)
          COLOR weiss
        ELSE
          COLOR schwarz
        ENDIF
        PBOX i%*8,j%*8,i%*8+7,j%*8+7
      ENDIF
    NEXT j%
  NEXT i%
  SHOWPAGE
  PAUSE pausp
RETURN
PROCEDURE init
  ' restore tum
  DO
    READ x
    EXIT if x=255
    READ y
    f(INT(bw%/2)+x,INT(bh%/2)+y)=1
  LOOP
RETURN

' PULSAR
DATA -1,0,0,0,1,0,2,0,3,0,-1,1,3,1,255
tum:
' TUMBLER
DATA -2,-3,-1,-3,1,-3,2,-3,-2,-2,-1,-2,1,-2,2,-2,-1,-1,1,-1
DATA -3,0,-1,0,1,0,3,0,-3,1,-1,1,1,1,3,1,-3,2,-2,2,2,2,3,2,255

