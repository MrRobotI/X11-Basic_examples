' Errechnet Beispiele zu Brefelds Vermutung
' Das Problem wird ausführlich hier diskutiert:
' http://www.brefeld.homepage.t-online.de/neunstellig-1.html
' Algorithm in X11-Basic
' by Markus Hoffmann 2015
'
'
FOR base%=2 TO 36 STEP 2
  CLR count%
  t=TIMER
  d$=@extend$("")
  PRINT "There are ";count%;" solutions for base ";base%;"; ";INT(TIMER-t);" Seconds."
NEXT base%
QUIT

FUNCTION extend$(a$)
  LOCAL i%,b$,c$
  ' print a$
  IF LEN(a$)=base%-1
    PRINT a$;" solution.base";base%
    INC count%
  ENDIF
  FOR i%=1 TO base%-1
    b$=RADIX$(i%,base%)
    IF INSTR(a$,b$)=0
      c$=a$+b$
      IF @isvalid(c$)
        c$=@extend$(c$)
        IF LEN(c$)>LEN(a$)
          RETURN c$
        ENDIF
      ENDIF
    ENDIF
  NEXT i%
  RETURN ""
ENDFUNCTION

FUNCTION isvalid(p$)
  LOCAL i%,a&
  FOR i%=1 TO LEN(p$)
    a&=@valradix&(LEFT$(p$,i%))
    ' print a,a mod i
    IF (a& MOD i%)
      RETURN 0
    ENDIF
  NEXT i%
  RETURN -1
ENDFUNCTION

FUNCTION valradix&(t$)
  LOCAL i%,a&
  CLR a&,o
  FOR i%=0 TO LEN(t$)-1
    o=PEEK(VARPTR(t$)+i%)
    IF o>=ASC("a")
      ADD o,10-ASC("a")
    ELSE IF o>=ASC("A")
      ADD o,10-ASC("A")
    ELSE
      SUB o,ASC("0")
    ENDIF
    a&=a&*base%+INT(o)
  NEXT i%
  '  print "valradix ",t$,"->",a
  RETURN a&
ENDFUNCTION
