' This program creates the scales for Wall-clocks.
' (c) Markus Hoffmann 2006
'

clocksize=80 ! Radius in mm
figfilename$=""
psfilename$=""
version$="1.06"

@ps_init("Bobouhr.fig")

@ps_dicke(1)
@ps_angle(0)
@ps_color(0)
@ps_circle(80,80,80,80)
@ps_pcircle(80,80,1,1)

@ps_font(22)
@ps_groesse(45)

@ps_tmode(1)

FOR i=1 TO 12
  @ps_text(80+45*sin(2*pi*i/12),85-45*cos(2*pi*i/12),str$(i))
NEXT i
FOR i=1 TO 60
  IF (i MOD 5)=0
    @ps_dicke(3)
  ELSE
    @ps_dicke(1)
  ENDIF
  @ps_line(80+55*sin(2*pi*i/60),80-55*cos(2*pi*i/60),80+60*sin(2*pi*i/60),80-60*cos(2*pi*i/60))
NEXT i
@ps_dicke(2)
FOR i=1 TO 12
  @quadrupol(80+72*sin(2*pi*i/12),80-72*cos(2*pi*i/12),2*pi*i/12)
NEXT i

@ps_close
QUIT
SYSTEM "fig2dev -L ps -z A4 -c "+figfilename$+" "+psfilename$
' system "rm "+figfilename$
SYSTEM "chmod 666 "+psfilename$

PROCEDURE quadrupol(mx,my,ori)
  LOCAL i
  DIM px(15)
  DIM py(15)

  ' polyline
  px(0)=-5
  py(0)=-6
  px(1)=5
  py(1)=-6
  px(2)=5
  py(2)=6
  px(3)=-5
  py(3)=6
  px(4)=-5
  py(4)=-6
  px(5)=-5
  py(5)=-4
  px(6)=-5
  py(6)=4
  px(7)=5
  py(7)=-4
  px(8)=5
  py(8)=4

  @ps_dicke(1)

  FOR i=0 TO 14
    ccx=cos(ori)*px(i)-sin(ori)*py(i)
    ccy=sin(ori)*px(i)+cos(ori)*py(i)
    px(i)=ccx
    py(i)=ccy
  NEXT i
  @ps_dicke(2)
  @ps_pcircle(mx+px(5),my+py(5),2,2)
  @ps_pcircle(mx+px(6),my+py(6),2,2)
  @ps_pcircle(mx+px(7),my+py(7),2,2)
  @ps_pcircle(mx+px(8),my+py(8),2,2)

  PRINT #1,"2 1 0 "+STR$(dicken)+" "+STR$(colorn)+" 7 0 0 -1 0.000 0 0 -1 0 0 5"
  FOR i=0 TO 4
    PRINT #1," "+STR$(INT(45*px(i)+45*mx))+" "+STR$(INT(45*py(i)+45*my));
  NEXT i
  PRINT #1
RETURN

PROCEDURE ps_init(figfilename$)
  OPEN "O",#1,figfilename$
  PRINT #1,"#FIG 3.2"
  PRINT #1,"Portrait"
  PRINT #1,"Center"
  PRINT #1,"Metric"
  PRINT #1,"A4"
  PRINT #1,"100.00"
  PRINT #1,"Single"
  PRINT #1,"-2"
  PRINT #1,"1200 2"
RETURN

PROCEDURE ps_close
  CLOSE #1
RETURN

PROCEDURE ps_line(x1,y1,x2,y2)
  @mmm
  PRINT #1,"2 1 0 "+STR$(dicken)+" "+STR$(colorn)+" 7 0 0 -1 0.000 0 0 -1 0 0 2"
  PRINT #1," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y2)
RETURN
PROCEDURE ps_dotline(x1,y1,x2,y2)
  @mmm
  PRINT #1,"2 1 2 "+STR$(dicken)+" "+STR$(colorn)+" 7 0 0 -1 3.000 0 0 -1 0 0 2"
  PRINT #1," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y2)
RETURN
PROCEDURE ps_box(x1,y1,x2,y2)
  @mmm
  PRINT #1,"2 1 0 "+STR$(dicken)+" "+STR$(colorn)+" 7 0 0 -1 0.000 0 0 -1 0 0 5"
  PRINT #1," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y1);
  PRINT #1," "+STR$(x2)+" "+STR$(y2)+" "+STR$(x1)+" "+STR$(y2);
  PRINT #1," "+STR$(x1)+" "+STR$(y1)
RETURN
PROCEDURE ps_circle(x1,y1,r1,r2)
  x2=x1+r1/2
  y2=y1+r2/2
  @mmm
  PRINT #1,"1 3 0 "+STR$(dicken)+" "+STR$(colorn)+" 7 50 -1 -1 0.000 1 "+STR$(anglen,6,6)+"";
  PRINT #1," "+STR$(x1)+" "+STR$(y1)+" "+STR$(45*r1)+" "+STR$(45*r2);
  PRINT #1," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y2)
RETURN
PROCEDURE ps_arc(cx,cy,r1,a1,a2)
  x1=cx+r1*cos(a1)
  y1=cy+r1*sin(a1)
  x2=cx+r1*cos((a2-a1)/2)
  y2=cy+r1*sin((a2-a1)/2)
  x3=cx+r1*cos(a2)
  y3=cy+r1*sin(a2)

  @mmm
  x3=INT(45*x3)
  y3=INT(45*y3)
  arctyp=1

  PRINT #1,"5 "+STR$(arctyp)+" "+STR$(linestyle)+" "+STR$(dicken)+" ";
  PRINT #1,STR$(colorn)+" 7 50 -1 -1 0.000 1 "+STR$(anglen,6,6)+"";
  PRINT #1," "+STR$(cx)+" "+STR$(cy);
  PRINT #1," "+STR$(x1)+" "+STR$(y1);
  PRINT #1," "+STR$(x2)+" "+STR$(y2);
  PRINT #1," "+STR$(x3)+" "+STR$(y3)
RETURN
PROCEDURE ps_filledarc(cx,cy,r1,a1,a2)
  x1=cx+r1*cos(a1)
  y1=cy+r1*sin(a1)
  x2=cx+r1*cos((a2-a1)/2)
  y2=cy+r1*sin((a2-a1)/2)
  x3=cx+r1*cos(a2)
  y3=cy+r1*sin(a2)

  @mmm
  x3=INT(45*x3)
  y3=INT(45*y3)
  arctyp=2

  PRINT #1,"5 "+STR$(arctyp)+" "+STR$(linestyle)+" "+STR$(dicken)+" ";
  PRINT #1,STR$(colorn)+" 7 50 -1 -1 0.000 1 "+STR$(anglen,6,6)+"";
  PRINT #1," "+STR$(cx)+" "+STR$(cy);
  PRINT #1," "+STR$(x1)+" "+STR$(y1);
  PRINT #1," "+STR$(x2)+" "+STR$(y2);
  PRINT #1," "+STR$(x3)+" "+STR$(y3)
RETURN
PROCEDURE ps_pcircle(x1,y1,r1,r2)
  LOCAL x2,y2
  x2=x1+r1/2
  y2=y1+r2/2
  @mmm
  PRINT #1,"1 3 0 "+STR$(dicken)+" "+STR$(colorn)+" 0 50 0 20 0.000 1 "+STR$(anglen,6,6)+"";
  PRINT #1," "+STR$(x1)+" "+STR$(y1)+" "+STR$(r1*45)+" "+STR$(r2*45);
  PRINT #1," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y2)
RETURN

PROCEDURE ps_pbox(x1,y1,x2,y2)
  @mmm
  PRINT #1,"2 1 0 "+STR$(dicken)+" "+STR$(colorn)+" 7 0 0 20 0.000 0 0 -1 0 0 5"
  PRINT #1," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y1);
  PRINT #1," "+STR$(x2)+" "+STR$(y2)+" "+STR$(x1)+" "+STR$(y2);
  PRINT #1," "+STR$(x1)+" "+STR$(y1)
RETURN
PROCEDURE ps_text(x1,y1,t$)
  @mmm
  PRINT #1,"4 "+STR$(tmoden)+" 0 100 0 "+STR$(fontn)+" "+STR$(groessen)+" "+STR$(anglen,6,6)+" 4 165 4830 "+STR$(INT(x1))+" "+STR$(INT(y1))+" "+t$+"\001"
RETURN
PROCEDURE mmm
  MUL x1,45
  MUL y1,45
  MUL x2,45
  MUL y2,45
  x1=INT(x1)
  x2=INT(x2)
  y1=INT(y1)
  y2=INT(y2)
RETURN

PROCEDURE ps_color(c)
  colorn=c
RETURN
PROCEDURE ps_pattern(c)
  patternn=c
RETURN
PROCEDURE ps_dicke(c)
  dicken=c
RETURN
PROCEDURE ps_angle(c)
  anglen=c/180*pi
RETURN
PROCEDURE ps_groesse(c)
  groessen=c
RETURN
PROCEDURE ps_tmode(c)
  tmoden=c
RETURN
PROCEDURE ps_font(c)
  fontn=c
RETURN
