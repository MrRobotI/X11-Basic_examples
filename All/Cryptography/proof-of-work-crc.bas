' Produce a message, which contains a variable part (a nonce), 
' which has a CRC checksum with many zeroes (like bitcoins).
' This algorithm is called: proof-of-work
'
' (c) by Markus Hoffmann, written in X11-Basic
'
'

difficulty%=0x3ffff
t$="[xxxx] Hello, this is the message."

PRINT "searching..."
FOR i=32 TO ASC("z")
  FOR j=32 TO ASC("z")
    FOR k=32 TO ASC("z")
      FOR l=32 TO ASC("z")
        POKE VARPTR(t$)+1,i
        POKE VARPTR(t$)+2,j
        POKE VARPTR(t$)+3,k
        POKE VARPTR(t$)+4,l
        c%=CRC(t$)
        f=((c% AND difficulty%)=0)
        EXIT if f
      NEXT l
      EXIT if f
    NEXT k
    PRINT i,j,k,l;CHR$(13);
    EXIT if f
    FLUSH
  NEXT j
  EXIT if f
NEXT i
PRINT
PRINT t$
a$=HEX$(CRC(t$),8)
PRINT "The CRC32 checksum is: "+a$
END
