' Displays Atari-ST 8*16 fixed Fonts (c) Markus Hoffmann
scale=2
weiss=GET_COLOR(65535,65535,0)
n=19
o=-1

x=0
y=0
text$=""
PRINT n,o
f$=PARAM$(2)
IF exist(f$)
  OPEN "I",#1,f$
  l=lof(#1)
  PRINT int(n*4096/l*100);"%"
  SEEK #1,n*4096+862+6*256
  f$=input$(#1,4096)
  CLOSE #1
  FOR i=0 TO 256
    text$=text$+CHR$(i)
  NEXT i
ENDIF
@text(text$)
ENDIF
INC o
OPEN "O",#1,"ST-font-8-16.fnt"
PRINT #1,f$;
CLOSE #1
QUIT
PROCEDURE text(t$)
  LOCAL i
  FOR i=0 TO LEN(t$)-1
    char=PEEK(VARPTR(t$)+i) and 255
    @char(x,y,char)
    VSYNC

    ADD x,8*scale
    IF x>=640
      x=0
      ADD y,16*scale+1
    ENDIF
  NEXT i
RETURN
PROCEDURE char(x,y,c)
  COLOR 0
  PBOX x,y,x+8*scale,y+16*scale
  COLOR weiss
  LOCAL i,j
  FOR i=0 TO 15
    FOR j=0 TO 7
      IF btst(PEEK(VARPTR(f$)+c+i*256),7-j)=0
        PBOX x+j*scale,y+i*scale,x+j*scale+scale,y+i*scale+scale-1
      ENDIF
    NEXT j
  NEXT i
RETURN
