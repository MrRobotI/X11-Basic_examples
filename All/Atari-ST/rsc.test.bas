' Opens Object trees from a ATARI ST resource file (*.RSC) and
' displays them. (c) Markus Hoffmann 2002
'
' Demonstrates the usage of RSRC_LOAD, GPRINT, RSRC_GADDR, FORM_CENTER
'                           FORM_DIAL, FORM_DO
'                           in X11-Basic
'
COLOR COLOR_RGB(1,0,0)
PBOX 0,0,640,400
COLOR COLOR_RGB(1,1,0)
GPRINT "You can use ATARI ST *.RSC-files..."
GPRINT "Test of the rsrc-functions..."
' v.1.11 (c) Markus Hoffmann
COLOR COLOR_RGB(0,1,0)

FOR i=0 TO 30
  CIRCLE 320,200,i*5
NEXT i
GPRINT "use the right mouse button TO skip this dialog."

i=0
WHILE LEN(PARAM$(i))
  INC i
WEND
f$=PARAM$(i-1)
PRINT i,f$
IF NOT EXIST(f$) OR UPPER$(RIGHT$(f$,4))<>".RSC"
  FILESELECT "load RSC...","./rsc/*.rsc","",f$
ENDIF
RSRC_LOAD f$
count=0
adr=RSRC_GADDR(15,count) ! Get the address of the first Free String
WHILE adr<>-1
  PRINT count,adr
  t$=SPACE$(20000)
  BMOVE adr,VARPTR(t$),1000
  SPLIT t$,CHR$(0),0,t$,a$
  PRINT count,adr,t$
  ~FORM_ALERT(1,T$) ! Display it as an ALERT box
  INC count
  adr=RSRC_GADDR(15,count) ! get the next address
WEND
count=0
adr=RSRC_GADDR(0,count) ! Get the address of the first object tree
WHILE adr<>-1
  ~FORM_CENTER(adr,x,y,w,h)      ! Center the tree on the screen and get its coordinates
  ~FORM_DIAL(0,x,y,w,h,x,y,w,h)  ! save the background
  ~FORM_DIAL(1,x,y,w,h,x,y,w,h)  !
  ~OBJC_DRAW(adr,0,-1,0,0,w,h)   ! Draw the object tree
  ret=FORM_DO(adr)               ! Manage User input
  ~FORM_DIAL(2,x,y,w,h,x,y,w,h)  !
  ~FORM_DIAL(3,x,y,w,h,x,y,w,h)  ! Restore the screen background
  PRINT "The dialog #";count;" was left with the object #";ret
  INC count
  adr=RSRC_GADDR(0,count) ! Get the address of the next object tree
WEND
RSRC_FREE ! frees the memory used by the Resource
QUIT
