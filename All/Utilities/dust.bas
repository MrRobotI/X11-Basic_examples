' DUST (Portierung von ATARI ST GFA-Basic Orginalversion 1989)
' Markus Hoffmann
'
' Printout a hex memory dump of all process memory available
' (demonstrateds the use of PEEK and VARPTR)
'
t$="Hello, this is my String..."
adr=VARPTR(t$)-128 ! start 128 bytes before the memory location of that string
eadr=adr+256
DO
  PRINT "$";HEX$(adr,8)'
  FOR i%=0 TO 15
    PRINT HEX$(PEEK(adr+i%) AND 255,2)'
  NEXT i%
  PRINT '
  FOR i%=0 TO 15
    a=PEEK(adr+i%)
    IF a>31
      PRINT CHR$(a);
    ELSE
      PRINT ".";
    ENDIF
  NEXT i%
  PRINT
  PAUSE 0.1
  ADD adr,16
  EXIT if adr>eadr
LOOP
QUIT
