' Konto.bas V.1.08
' Kontofuehrung grafisch angezeigt  (c) Markus Hoffmann
' Letzte Bearbeitung 20.12.1997
' Letzte Bearbeitung 14.12.2002

' Die Dateien mit den Kontobewegungen muessen folgendes Format haben:
'
' DATUM,        UMSATZ,  KONTOSTAND, Kommentar
' 02.05.1997,   1300.00,    2593.99, "Gehalt"
' 02.06.1997,   1300.00,    3893.99, "Gehalt"
' 10.06.1997,  -1893.00,    2000.99, "Restsparen"
' 30.06.1997,      1.30,    2002.29, "Zinsen"
' 01.07.1997,    -32.87,    1969.42, "Telekom-Rechnung"
' ...
'
'
'

'########## Hauptprogramm ##########
'##########     begin     ##########

tty=0

offset1=-70
faktor1=0.2
offset2=-3300
faktor2=1
DIM mon$(13)
RESTORE mdata
FOR i=1 TO 12
  READ mon$(i)
NEXT i
mdata:
DATA Jan,Feb,Mar,Apr,Mai,Jun,Jul,Aug,Sep,Okt,Nov,Dez

grau=GET_COLOR(65535/2,65535/2,65535/2)
weiss=GET_COLOR(65535,65535,65535)
rot=GET_COLOR(65535,0,0)
gelb=GET_COLOR(65535,65535,0)
gruen=GET_COLOR(0,65535,0)
blau=GET_COLOR(10000,10000,65535)
schwarz=GET_COLOR(0,0,0)

'
' Dateneingabe f�r Konto:
'
konto$="konto.dat"

maxkontostand=5000
maxkontostand2=5000
sdatum$="01.01.1992"
edatum$=date$
bx=0
by=240
bw=740
bh=220
bx2=0
by2=10
bw2=740
bh2=200

SIZEW ,bw,bh+bh2+100
PAUSE 0.5
CLEARW
SHOWPAGE

COLOR grau
LINE bx,by+bh,bx+bw,by+bh
LINE bx,by,bx+bw,by
LINE bx,200,bx+bw,200
COLOR gelb
SETFONT "*-Courier-*34*"
TEXT 150,500,"Kontostand "+date$
DEFTEXT 1,0.04,0.04,0
SETFONT "*Courier*10*"

IF tty
  INPUT "Welches Konto anzeigen ? 1=DB, 2=Sparda, 3=SPK, 4=ABBRUCH",fa
ELSE
  fa=form_alert(1,"[0][KONTO.BAS||Welches Konto anzeigen ?][DB|Sparda|andere|Quit]")
ENDIF
IF fa=4
  PRINT "Dann ende ..."
  QUIT
ELSE if fa=2
  konto$="sparda.dat"
  maxkontostand=6000
ELSE if fa=3
  FILESELECT "Konto ausw�hlen...","./*.dat","konto.dat",konto$
  IF konto$=""
    QUIT
  ENDIF
ENDIF
IF NOT EXIST(konto$)
  IF form_alert(2,"[3]["+konto$+"|existiert nicht !||File anlegen ?][Neu|ABBRUCH]")=2
    PRINT konto$;" existiert nicht !"
    END
  ELSE
    OPEN "O",#1,konto$
    PRINT #1,"10000,"+date$
    PRINT #1,date$+",0,0,"+""""+"Eroeffnung des Kontos "+""""
    CLOSE #1
  ENDIF
ENDIF
OPEN "I",#1,konto$
INPUT #1,maxkontostand
INPUT #1,sdatum$
INPUT #1,offset2
maxkontostand2=maxkontostand
count=0
sjahr=VAL(RIGHT$(sdatum$,4))
ejahr=VAL(RIGHT$(edatum$,4))
faktor1=bw/(JULIAN(edatum$)-julian(sdatum$))
FOR j=sjahr TO ejahr
  FOR m=1 TO 12
    x=(JULIAN("01."+STR$(m,2,2,1)+"."+STR$(j))-julian(sdatum$))*faktor1+bx
    x2=(JULIAN("01."+STR$(m,2,2,1)+"."+STR$(j))-julian(sdatum$))*faktor2+offset2+bx
    COLOR grau
    LINE x2,@ky2(0)-3,x2,@ky2(0)
    LINE x,@ky(0)-3,x,@ky(0)
    COLOR gelb
    TEXT x2+4,@ky2(0)+10,mon$(m)
  NEXT m
  COLOR grau
  LINE x,@ky(maxkontostand),x,@ky(0)
  COLOR gelb
  TEXT x,by-10,STR$(j+1))
  COLOR grau
  FOR y=0 TO maxkontostand STEP INT(maxkontostand/1000/5)*1000
    LINE x-2,@ky(y),x+2,@ky(y)
    TEXT x+2,@ky(y),STR$(y)
  NEXT y
NEXT j

COLOR grau
FOR y=0 TO maxkontostand STEP INT(maxkontostand/1000/10)*1000
  mmmy=@ky2(y)
  LINE 40,mmmy,bx2+bw2,mmmy
  TEXT 4,mmmy,STR$(y)
NEXT y
SHOWPAGE
COLOR weiss
CLR oldx,oldx2
oldy=@ky(0)
oldy2=@ky2(0)
CLS

WHILE NOT EOF(#1)
  INPUT #1,d$,a,s,c$
  x=(JULIAN(d$)-julian(sdatum$))*faktor1+bx
  x2=(JULIAN(d$)-julian(sdatum$))*faktor2+offset2+bx
  y=@ky(s)
  y2=@ky2(s)

  COLOR grau
  IF x>bx AND x<bw+bx
    LINE x,@ky(0),x,y
  ENDIF
  IF x2>bx AND x2<bw+bx
    LINE x2,@ky2(0),x2,y2
  ENDIF
  IF s>0
    COLOR gruen
  ELSE
    COLOR rot
  ENDIF
  IF x>bx AND x<bw+bx
    LINE oldx,oldy,x,oldy
    LINE x,oldy,x,y
  ENDIF
  IF x2>bx AND x2<bw+bx
    LINE oldx2,oldy2,x2,oldy2
    LINE x2,oldy2,x2,y2
  ENDIF
  COLOR blau
  IF x>bx AND x<bw+bx
    LINE x,@ky(0),x,@ky(ABS(a))
  ENDIF
  IF x2>bx AND x2<bw+bx
    LINE x2,@ky2(0),x2,@ky2(ABS(a))
  ENDIF
  oldx=x
  oldx2=x2
  oldy=y
  oldy2=y2
  IF (count MOD 100)=0
    SHOWPAGE
  ENDIF
  INC count
WEND
IF tty
  CLS
  SHOWPAGE
  @ttyhardcopy
ELSE
  ALERT 0,"Fertig !",1,"OK",dummy
ENDIF
QUIT

' absolutes Ende (des Hauptprogramms)
DEFFN ky(kky)=by+bh-kky/maxkontostand*bh
DEFFN ky2(kky)=by2+bh2-kky/maxkontostand2*bh2

FUNCTION monday(n)
  IF n=1 OR n=3 OR n=5 OR n=7 OR n=8 OR n=10 OR n=12
    RETURN 31
  ELSE if n=2
    RETURN 28
  ELSE if n=4 OR n=6 OR n=9 OR n=11
    RETURN 30
  ELSE
    RETURN -1
  ENDIF
ENDFUNCTION

FUNCTION mondaysum(n)
  LOCAL i,s,o
  s=0
  FOR i=1 TO n
    o=@monday(i)
    IF o<0
      RETURN o
    ENDIF
    s=s+o
  NEXT i
  RETURN s
ENDFUNCTION
PROCEDURE ttyhardcopy
  LOCAL bbp,bx,by,bw,bh
  bbp=8
  bx=0
  by=0
  bw=640
  bh=400

  DIM zeil(16)
  DIM old(16)

  FOR y=0 TO 399 STEP 16
    FOR x=0 TO bw-1 STEP 8
      flag=0
      FOR j=0 TO 15
        a=0

        FOR i=0 TO 7
          u=point(x+i,y+j)
          IF bbp=24
            r=u and 255
            g=(u/256) and 255
            b=(u/256/256) and 255
            u=sqrt(r*r+g*g+b*b)
            IF random(u)>80 OR u>350
              a=bset(a,7-i)
              flag=1
            ENDIF
          ELSE
            IF u AND 1
              a=bset(a,7-i)
              flag=1
            ENDIF
          ENDIF
        NEXT i
        zeil(j)=a
      NEXT j
      IF flag=0
        PRINT " ";
      ELSE
        rflag=0
        FOR i=0 TO 15
          IF old(i)<>zeil(i)
            rflag=1
          ENDIF
        NEXT i
        IF rflag
          PRINT chr$(27);"[255;";
          FOR j=0 TO 14
            PRINT str$(zeil(j))+";";
            old(j)=zeil(j)
          NEXT j
          old(15)=zeil(15)
          PRINT str$(zeil(15))+"z";
        ENDIF
        PRINT chr$(255);
      ENDIF
    NEXT x
    FLUSH
    IF bw<640
      PRINT
    ENDIF
  NEXT y
RETURN
