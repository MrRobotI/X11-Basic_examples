ECHO off
' ######################################################### Version 1.00
' ##	Programm berechnet den Wochentag eines Datums    ## letzte Bearbeitung:
' ## 					     24.09.1993  ## 26.09.1993
' ############### Markus Hoffmann #########################
'
'	PROGRAM datum1
'
IF parm$<>""
  in$=parm$
ELSE
  PRINT "Bitte das Datum eingeben und zwar in der Form:"
  PRINT "TT.MM.JJJJ : (z.B. ";date$;" )";
  INPUT "",in$
  IF in$=""
    in$=date$
  ENDIF
ENDIF
tag=VAL(LEFT$(in$,2))
xtag=VAL(LEFT$(date$,2))
jahr=VAL(RIGHT$(in$,4))
xjahr=VAL(RIGHT$(date$,4))
monat=VAL(MID$(in$,4,2))
xmonat=VAL(MID$(date$,4,2))

wochentage$()=["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag","Glueckstag"]
tage()=[0,31,29,31,30,31,30,31,31,30,31,30,31]

'  F�r Julianischen Kalender (beginnt mit M�rz)
IF monat=1 OR monat=2
  DEC jahr
  ADD monat,12
ENDIF
SUB monat,2

'   Das Jahrhundert nach c[entury]
'
c=jahr div 100
'
'  Jahreszahl ohne Jahrhundert nach a[nno]
'
a=jahr MOD 100
'
'        v+--- Das ergibt den Wochentag (0=Sonntag .... 6=Samstag)
'	       ===================================================
wtag=(tag+INT(2.6*monat-0.1)+a+INT(a/4)+INT(c/4)-2*c) MOD 7
5:
zflg=(Jahr>xjahr) or (jahr=xjahr and (monat>xmonat or (monat=xmonat and tag>xtag)))
wjahr=jahr
IF monat>10
  wjahr=jahr+1
ENDIF
PRINT " Der "'tag;".";1+((monat-1+2) MOD 12);".";wjahr''
IF jahr=xjahr AND monat=xmonat AND tag=xtag
  PRINT "ist"'
ELSE
  IF zflg
    PRINT "wird sein"'
  ELSE
    PRINT "war"'
  ENDIF
ENDIF
PRINT " ein ";wochentage$(wtag);"."
IF wtag=5 AND tag=13
  END
ENDIF
'
' Freitag, den 13. berechnen:
'
IF tag<13
  DEC monat
ENDIF
tag=13
DO
  INC monat
  IF (monat>12)
    INC jahr
    monat=1
  ENDIF
  c=jahr div 100
  a=jahr mod 100
  wtag=(tag+INT(2.6*monat-0.1)+a+INT(a/4)+INT(c/4)-2*c) mod 7
  IF wtag=5
    GOTO 5
  ENDIF
LOOP
'
'   ### und nun das fakultative ....
END
