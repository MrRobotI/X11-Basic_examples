' converts .fnt files into C source code for inclution in programs
'
OPEN "I",#1,"spat-a.fnt"
PRINT "const unsigned char fontdata816[]={"
FOR j=0 TO 255
  PRINT "  ";
  FOR i=0 TO 15
    a=INP(#1)
    PRINT "0x";HEX$(a,2,2,1);
    IF j<>255 OR i<>15
      PRINT ",";
    ENDIF
  NEXT i
  PRINT "  /*";j;"*/"
NEXT j
PRINT "};"
CLOSE
QUIT
