' It is possible to modify the X11-Basic preferences under Android by
' reading or writing the files under this directory:

' chdir "/data/data/net/sourceforge/x11basic"

' This little demonstration program just lists and prints out what it finds there.
' It is up to you to do something useful with it...
'

a$=FSFIRST$("/data/data/net.sourceforge.x11basic")
f$="net.sourceforge.x11basic_preferences.xml"
WHILE LEN(a$)
  PRINT a$
  a$=FSNEXT$()
WEND
PRINT SYSTEM$("cd /data/data/net.sourceforge.x11basic/shared_prefs ; cat myappraterdialog.xml")
PRINT "------"
PRINT SYSTEM$("cd /data/data/net.sourceforge.x11basic/shared_prefs ; cat "+f$)
PRINT "------"
PRINT SYSTEM$("cd /data/data/net.sourceforge.x11basic/shared_prefs ; ls ")
PRINT "------"
PRINT SYSTEM$("cd /data/data/net.sourceforge.x11basic ; cat cache/* ")
END
