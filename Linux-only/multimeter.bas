' gets the Values from a Multimeter
' e.g. the METEX M-4650CR         (c) Markus Hoffmann 2004
'
'
'
devicename$="/dev/ttyS1"
PRINT "Open device ",devicename$
OPEN "UX:1200,N,8,1",#1,devicename$

PRINT #1,"D";
PAUSE 1
WHILE 1
  PAUSE 1
  PRINT INP(#1)
WEND
CLOSE
QUIT
