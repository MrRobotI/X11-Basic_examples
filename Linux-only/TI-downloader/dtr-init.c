/* dtr-init.C                                           (c) Markus Hoffmann
  helper application to control the DTR and RTS lines of a serial device
  V.1.00 03.2007
*/

/* This file is part of X11BASIC, the basic interpreter for Unix/X
 * ======================================================================
 * X11BASIC is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */

/* This is quick and dirty and needs to be cleaned up */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//#include <ctype.h>
#include <errno.h>
//#include <stdarg.h>
#include <signal.h>
//#include <unistd.h>
#include <fcntl.h>
//#include <sys/types.h>
#include <sys/stat.h>
//#include <sys/wait.h>

#define _GNU_SOURCE
#include <getopt.h>
#ifdef __linux__
#include <termio.h>
#else
#include <termios.h>
#endif /* of __linux__ */

#ifndef FALSE
  #define FALSE    0
  #define TRUE     (!FALSE)
#endif

/* global variables */

char device[256]="/dev/ttyS1";	
int dorts=0;

int exist(char *filename) {
  struct stat fstats;
  int retc=stat(filename, &fstats);
  if(retc==-1) return(FALSE);
  return(TRUE);
}

int term_raise_dtr(int fd) {
	int rval=0, r;
	do { /* dummy */
#ifdef __linux__
		{
			int opins = TIOCM_DTR;

			r = ioctl(fd, TIOCMBIS, &opins);
			if ( r < 0 ) {
				rval = -1;
				break;
			}
		}
#else
		r = tcsetattr(fd, TCSANOW, &term.currtermios[i]);
		if ( r < 0 ) {
			/* FIXME: perhaps try to update currtermios */
			rval = -1;
			break;
		}
#endif /* of __linux__ */
  } while (0);
  return rval;
}
int term_raise_rts(int fd) {
	int rval=0, r;
	do { /* dummy */

#ifdef __linux__
		{
			int opins = TIOCM_RTS;

			r = ioctl(fd, TIOCMBIS, &opins);
			if ( r < 0 ) {
				rval = -1;
				break;
			}
		}
#else
#endif /* of __linux__ */
  } while (0);
  return rval;
}
/***************************************************************************/

int term_lower_dtr(int fd) {
	int rval=0, r;
	do { /* dummy */

#ifdef __linux__
		{
			int opins = TIOCM_DTR;

			r = ioctl(fd, TIOCMBIC, &opins);
			if ( r < 0 ) {
				rval = -1;
				break;
			}
		}
#else
		{
			struct termios tio;

			r = tcgetattr(fd, &tio);
			if ( r < 0 ) {
				rval = -1;
				break;
			}
			term.currtermios[i] = tio;
			
			cfsetospeed(&tio, B0);
			cfsetispeed(&tio, B0);
			
			r = tcsetattr(fd, TCSANOW, &tio);
			if ( r < 0 ) {
				rval = -1;
				break;
			}
		}
#endif /* of __linux__ */
  } while (0);
  return rval;
}
int term_lower_rts(int fd) {
	int rval=0, r;
	do { /* dummy */
#ifdef __linux__
		{
			int opins = TIOCM_RTS;

			r = ioctl(fd, TIOCMBIC, &opins);
			if ( r < 0 ) {
				rval = -1;
				break;
			}
		}
#else
#endif /* of __linux__ */
  } while (0);
  return rval;
}

void intro(){
  puts("***************************************************************");
  puts("*    dtr-init        by Markus Hoffmann      2007 (c)         *");
  puts("*                                                             *");
  puts("***************************************************************");
  puts("");
}

void usage(){
  puts("\n Usage:\n ------ \n");
  printf(" %s [-h] [<device>] --- init the dtr lines of serial device [%s]\n\n","dtr-init",device);
  puts("-h --help           --- Usage");
  puts("--rts               --- Use rts insted of dtr");
}
void kommandozeile(int anzahl, char *argumente[]) {
  int count,quitflag=0;
  char buffer[100];

  /* Kommandozeile bearbeiten   */
  for(count=1;count<anzahl;count++) {
    if (strcmp(argumente[count],"-h")==FALSE) {
      intro();
      usage();
      quitflag=1;
    } else if (strcmp(argumente[count],"--rts")==FALSE) {
      dorts=1;
    } else if (strcmp(argumente[count],"--help")==FALSE) {
      intro();
      usage();
      quitflag=1;
    } else {
        strcpy(device,argumente[count]);
    }
   }
   if(quitflag) exit(0);
}

main(int anzahl, char *argumente[]) {
  int fd;
  if(anzahl<2) {    /* Kommandomodus */
    intro();
  } else {
    kommandozeile(anzahl, argumente);    /* Kommandozeile bearbeiten */
    if(exist(device)) {
      fd=open(device, O_RDWR | O_NONBLOCK);
      if(fd<0) { 
        printf("cannot open %s: %s\n",device, strerror(errno));
        exit(0);
      }  
      if(dorts) {
        term_lower_rts(fd);
        sleep(1);
        term_raise_rts(fd);
      } else {
        term_lower_dtr(fd);
        sleep(1);
        term_raise_dtr(fd);
      }
      close(fd);
    } else printf("ERROR: %s not found !\n",device);
  }
}
