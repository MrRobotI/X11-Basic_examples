'
' Example program to demonstrate the Usage together with MQTT
'
' This implements a simple dashbord to display the values
' produced by a temperature sensor.
' (c) Markus Hoffmann 2019 
' 
'
BROKER "tcp://localhost:1883"   ! This will use the local broker, e.q. mosquitto

DIM x(100),y(100),tops$(100)
DIM fl(100) ! field length
anztops=0
CLS
READ a
WHILE a>0
  READ b
  READ t$
  READ f
  x(anztops)=a
  y(anztops)=b
  fl(anztops)=f
  tops$(anztops)=t$
  INC anztops
  SUBSCRIBE t$,,callback
  READ a
wend

PRINT AT(1,1);"Sensor Dashboard"

PRINT AT(3,1);"Temperature:     °C"
PRINT AT(4,1);"Humidity:        %"
PRINT AT(5,1);"Dew Point:       °C"
PRINT AT(7,1);"Status:"
DO
  PAUSE 1
  a$=INKEY$
  IF LEN(a$)
    PUBLISH "CMD",a$,2
  ENDIF
LOOP
QUIT


DATA 15,3,"TEMPERATURE",3
DATA 15,4,"HUMIDITY",3
DATA 15,5,"DEWPOINT",3
DATA 18,2,"ACTIVITY",1
DATA 18,9,"COUNTER",8
DATA 15,7,"STATUS",20
DATA 20,1,"TIME",8
DATA 1,8,"CMD",10
DATA -1

PROCEDURE callback(topic$,message$)
  LOCAL i
  FOR i=0 TO anztops-1
    IF topic$=tops$(i)
      PRINT AT(y(i),x(i));SPACE$(fl(i));
      PRINT AT(y(i),x(i));message$
      FLUSH
    ENDIF
  NEXT i
RETURN
