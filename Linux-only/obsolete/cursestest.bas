text$="Test, das ist der Test"+CHR$(10)+CHR$(0)

lib$="/lib/i386-linux-gnu/libncurses.so"

IF NOT EXIST(lib$)
  PRINT "ncurses lib not found."
  QUIT
ENDIF
LINK #1,lib$
CALL SYM_ADR(#1,"initscr")
CALL SYM_ADR(#1,"cbreak")
CALL SYM_ADR(#1,"noecho")
CALL SYM_ADR(#1,"nonl")
stdscr=SYM_ADR(#1,"stdscr")
CALL SYM_ADR(#1,"intrflush"),L:stdscr,0
CALL SYM_ADR(#1,"keypad"),L:stdscr,1
lptr=SYM_ADR(#1,"LINES")
cptr=SYM_ADR(#1,"COLS")
PAUSE 1
CALL SYM_ADR(#1,"printw"),L:VARPTR(text$)

FOR i=0 TO 16
  CALL SYM_ADR(#1,"attron"),2^(16+i)
  CALL SYM_ADR(#1,"printw"),L:varptr(text$)
  CALL SYM_ADR(#1,"attroff"),2^(16+i)
NEXT i
' newwin=CALL(sym_adr("subwin"),stdscr,1,21,10,35)
CALL SYM_ADR(#1,"refresh")
c=CALL(SYM_ADR(#1,"getch"))
CALL SYM_ADR(#1,"endwin")
PRINT "c=";c
PRINT "Screen: Lines: ";LPEEK(lptr);" Columns: ";LPEEK(cptr)
UNLINK #1
QUIT
