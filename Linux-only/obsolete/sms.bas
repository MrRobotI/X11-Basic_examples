#!/bin/xbasic
' sms.bas V.1.01 (c) Markus Hoffmann 1999
' Filterprogramm fuer Emails.
' Versucht im Wesentlichen, den Inhbalt einer Email so zu komprimieren,
' dass er zwar noch lesbar ist, aber weniger Zeichen benoetigt.
' Ausserdem wird verhindert, dass persoenliche Daten wie Adresse etc.
' Als SMS verschickt werden. Man weiss ja nie, wo das alles mitgeloggt wird.
'
'
' Mailumleitung mithilfe von procmail:
'
' ============= .procmailrc ===================
' :0 c
' * ^Subject:.SMS*
' | ( cat > /tmp/sendme.sms ; /home/user/bin/sms.bas ; rm /tmp/sendme.sms )
' :0
' * ^Subject:.SMS*
' /home/user/mail/SMSmail
' ==============================================
' Mails, deren Subject mit SMS: anfaengt, werden verschickt und in separatem
' Mail-Ordner SMSmail abgelegt
' Alle anderen Mails gehen den normalen Weg.
' Procmail laeuft uebrigens unter dem Useraccount.
'
'
' Ein Protokoll der versendeten SMS mit Status, ob es geklappt hat, wird
' angelegt
'
'
' benoetigt sendsms
'
nummer$="0173-69xxxxx"
protokoll$=ENV$("HOME")+"/mail/sendsms.log"
sendme$="/tmp/sendme.sms"
smsreturn$="/tmp/sendsms.return.html"
spammail=FALSE

st=0
data$=""
IF NOT EXIST(sendme$)
  PRINT "Achtung: keine Mail gefunden!?? "+sendme$
  QUIT
ENDIF
OPEN "I",#1,sendme$
WHILE NOT EOF(#1)
  LINEINPUT #1,t$
  SPLIT t$,":",TRUE,a$,b$
  IF UPPER$(a$)="FROM"
    from$=b$
    ofrom$=b$
  ELSE IF UPPER$(a$)="SUBJECT"
    subject$=b$
  ELSE IF UPPER$(a$)="TO"
    to$=TRIM$(b$)
  ENDIF
  IF t$=""
    st=1
  ENDIF
  IF st=1
    data$=data$+TRIM$(t$)+" "
  ENDIF
WEND
CLOSE #1
subject$=TRIM$(subject$)

from$=REPLACE$(from$,CHR$(34),"")
from$=REPLACE$(from$,"'","")

SPLIT from$,"<germnews@mathematik.uni-ulm.de>",TRUE,a$,b$
from$=TRIM$(a$+b$)

data$=from$+":"+subject$+";"+data$

READ a$,b$
WHILE a$<>""
  data$=REPLACE$(data$,a$,b$)
  READ a$,b$
WEND
data$=REPLACE$(data$,CHR$(34),":")
data$=REPLACE$(data$,"'",":")

text$=data$
text$=LEFT$(TRIM$(text$),159)

' Spamfilter:
RESTORE spamdata
READ a$
WHILE a$<>""
  IF GLOB(UPPER$(TRIM$(to$)),UPPER$(TRIM$(a$)))
    spammail=TRUE
  ENDIF
  READ a$
WEND

IF spammail=0
  ' system  "sendsms -o "+smsreturn$+" "+nummer$+" "+chr$(34)+text$+chr$(34)
  SYSTEM "txt2sms "+nummer$+" "+ENCLOSE$(text$)

  ' Antwort auswerten

  nosend=1
  IF EXIST(smsreturn$)
    OPEN "I",#1,smsreturn$
    IF NOT EOF(#1)
      LINEINPUT #1,status$
      SPLIT status$,"200",0,a$,b$
      IF LEN(b$)
        nosend=0
      ENDIF
    ENDIF
    CLOSE #1
  ENDIF
  IF nosend
    ' Mail ueber Errormeldung ??
  ENDIF
ELSE
  status$="SPAMFILTER: not sent."
ENDIF
SYSTEM "rm -f "+smsreturn$
OPEN "A",#1,protokoll$
PRINT #1,DATE$+" "+TIME$+" To: "+CHR$(34);
PRINT #1,to$+CHR$(34)+" From: "+CHR$(34);
PRINT #1,ofrom$+CHR$(34)+" Text: "+text$+" Status: "+status$
CLOSE #1
SYSTEM "chmod 600 "+protokoll$
QUIT

edata:
DATA "Hallo","Hi"
DATA "~","-"
DATA "!!","!","??","?","==","=","--","-","**","*"
DATA "und","&"
DATA " and "," & "
DATA " ueber "," � "
DATA " �ber "," � "
DATA " von "," v "
DATA " oder "," o "
DATA " mit "," m "
DATA " Zeit "," Zt "
DATA " bei "," b "
DATA " fuer "," f "
DATA " f�r "," f "
DATA "gleich","="
DATA "plus","+"
DATA "minus","-"
DATA " for "," 4 "
DATA " to "," 2 "
DATA " you "," U "
DATA " You "," U "
DATA "acht","8"
DATA "eins","1"
DATA "zwei","2"
DATA "drei","3"
DATA "vier","4"
DATA "neun","9"
DATA "null","0"
DATA "Montag ","Mo "
DATA "Dienstag ","Di "
DATA "Mittwoch ","Mi "
DATA "Donnerstag ","Do "
DATA "Freitag ","Fr "
DATA "Samstag ","Sa "
DATA "Sonntag ","So "

DATA "Markus","M-"
DATA "markus","M-"
DATA "Hoffmann","H-"
DATA "hoffmann","H-"
DATA "Hoffman","H-"
DATA "hoffman","H-"
DATA "Herr ","Hr "
DATA "Liebe","L-"
DATA "Bonn","BN"
DATA "Physik","P-"
DATA "Nussallee","N-"
DATA "Fachschaft","F-"
DATA "Heidelberg","HD"
DATA "Deutschland ","D "
DATA " Germany "," D "
DATA " germany "," D "

DATA "Sehr geehrte","Sg-"
DATA "Gruss","xx"
DATA "Gruesse","xxx"
DATA "[","(","{","(","]",")","}",")"

DATA "Physikalisches Institut","PI"
DATA "Universitaet","Uni"
DATA "physik.uni-bonn.de",""
DATA "uni-bonn.de",""
DATA ".de>",".>"

DATA "0 MESZ","0"
DATA "<HTML>",""

DATA "This is a multi-part message in MIME format.",""
DATA "This is a MIME-encapsulated message",""
DATA "Content-Type:",""
DATA "Content-Transfer-Encoding:",""
DATA "",""

spamdata:
' To-Filter
DATA "*undisclosed-recipients*"
DATA "*Undisclosed*Recipients*"
DATA "<amira12a@telenowa.de>"
DATA "tedoopie47@planet.com.mx"
DATA "wessa@ucs.com.tw"
DATA "Verteiler1*@mailings.gmx.de"
DATA "SR56qr@virtuale.it"
DATA ""
