' Make a DVD out of a set of .mpg and other video Files
' (c) Markus Hoffmann 2004
'
'
' needs dvdauthor and growisofs and transcode
'
dvdauthorxml$="dvdauthor.xml"
CLR anzvideos
DIM vob$(100),in$(100)

flags$="-c -q 6 -4 2 -2 1 -K file=matrix.txt"
asr$="--export_asr 2"
bitr$="8500"
' bitr$="4000"
i=1

WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF PARAM$(i)="--help" OR PARAM$(i)="-h"
      @intro
      @using
    ELSE IF PARAM$(i)="--version"
      @intro
      QUIT
    ELSE
      PRINT "Unknown Option: ";PARAM$(i)
    ENDIF
  ELSE
    IF @exist(PARAM$(i))
      IF right$(PARAM$(i),4)<>".bas"
        in$(anzvideos)=PARAM$(i)
        INC anzvideos
      ENDIF
    ELSE
      PRINT PARAM$(i);" does not exist. skipped"
    ENDIF
  ENDIF
  INC i
WEND
PRINT "got ";anzvideos;" Files."
IF anzvideos=0
  QUIT
ENDIF

OPEN "O",#1,"matrix.txt"
PRINT #1,"# High resolution INTRA table"
PRINT #1,"8,16,18,20,24,25,26,30"
PRINT #1,"16,16,20,23,25,26,30,30"
PRINT #1,"18,20,22,24,26,28,29,31"
PRINT #1,"20,21,23,24,26,28,31,31"
PRINT #1,"21,23,24,25,28,30,30,33"
PRINT #1,"23,24,25,28,30,30,33,36"
PRINT #1,"24,25,26,29,29,31,34,38"
PRINT #1,"25,26,28,29,31,34,38,42"
PRINT #1,"# TMPEGEnc NON-INTRA table"
PRINT #1,"16,17,18,19,20,21,22,23"
PRINT #1,"17,18,19,20,21,22,23,24"
PRINT #1,"18,19,20,21,22,23,24,25"
PRINT #1,"19,20,21,22,23,24,26,27"
PRINT #1,"20,21,22,23,25,26,27,28"
PRINT #1,"21,22,23,24,26,27,28,30"
PRINT #1,"22,23,24,26,27,28,30,31"
PRINT #1,"23,24,25,27,28,30,31,33"
CLOSE #1

SYSTEM "mkdir -v dvd"

' Files convertieren
FOR i=0 TO anzvideos-1
  IF right$(in$(i),4)=".vob"
    vob$(i)=in$(i)
  ELSE
    vob$(i)=in$(i)+".o.vob"
    IF not @exist(vob$(i))
      ' t$="nice transcode -i "+in$(i)+" -V -w "+bitr$+" --encode_fields b "+asr$+" "
      ' t$=t$+"-F 8,"+chr$(34)+flags$+chr$(34)+" -y mpeg2enc,mp2enc -b 224 -m "
      ' t$=t$+in$(i)+".o -o "+in$(i)+".o --print_status 10"
      t$="nice transcode -i "+in$(i)+" -V -E 44100 -w "+bitr$+" --encode_fields b "
      t$=t$+"-F 8,"+CHR$(34)+flags$+CHR$(34)+" -y mpeg2enc,mp2enc  -m "
      t$=t$+in$(i)+".o -o "+in$(i)+".o --print_status 10 --export_prof dvd"
      PRINT t$
      SYSTEM t$
      ' system "nice mplex -r 10000 -f 8 -S 4400 "+in$(i)+".o.m2v "+in$(i)+".o.mpa -o "+vob$(i)
      SYSTEM "nice mplex -f 8 -S 4400 "+in$(i)+".o.m2v "+in$(i)+".o.mpa -o "+vob$(i)
      IF @exist(vob$(i))
        SYSTEM "rm -f "+in$(i)+".o.m2v "+in$(i)+".o.mpa"
      ENDIF
    ENDIF
  ENDIF
NEXT i
SYSTEM "rm -f matrix.txt"

' Image creieren

t$="nice dvdwizard "
FOR i=0 TO anzvideos-1
  t$=t$+"-t auto -c 300 "+vob$(i)+" "
NEXT i
PRINT t$
SYSTEM t$

' brennen
PRINT "*** burning..."
PRINT "growisofs  -Z /dev/dvd  -V dvd -dvd-video dvd/ --dvd-compat"
SYSTEM "growisofs  -Z /dev/dvd  -V dvd -dvd-video dvd/ --dvd-compat"

QUIT

' Menues etc vorbereiten

OPEN "O",#1,dvdauthorxml$
PRINT #1,"<dvdauthor>"
PRINT #1,"    <vmgm />"
IF domenu
  PRINT #1,"<menus>"
  PRINT #1," <subpicture lang="+CHR$(34)+"german"+CHR$(34)+" />"
  PRINT #1,"</menus>"
ENDIF
PRINT #1,"    <titleset>"
PRINT #1,"	  <titles>"
PRINT #1,"	      <pgc>"
FOR i=0 TO anzvideos-1
  PRINT #1,"<vob file="+CHR$(34)+vob$(i)+CHR$(34);
  IF i<anzvideos-1
    PRINT #1," pause="+CHR$(34)+"2"+CHR$(34);
  ENDIF
  PRINT #1," />"
NEXT i
PRINT #1,"	      </pgc>"
PRINT #1,"	  </titles>"
PRINT #1,"    </titleset>"
PRINT #1,"</dvdauthor>"
CLOSE #1

' Image creieren

' dvdwizard -t auto -c 300 20041123_230712.mpg.o.vob -t auto -b back.ppm Chanson.mpg.o.vob

SYSTEM "dvdauthor -o dvd -x "+dvdauthorxml$
SYSTEM "chown -R root:root dvd"
SYSTEM "chmod -R 0755 dvd"
SYSTEM "chmod 0644 dvd/*_TS/*"

' brennen
PRINT "*** burning..."
SYSTEM "growisofs  -Z /dev/dvd  -V dvd -dvd-video dvd/ --dvd-compat"
QUIT

FUNCTION exist(f$)
  ' exist does not work for Files larger than 2 GB. Dont know why.
  LOCAL t$
  IF EXIST(f$)
    RETURN TRUE
  ELSE
    t$=SYSTEM$("ls "+f$)
    IF LEN(t$)
      RETURN TRUE
    ENDIF
  ENDIF
  RETURN 0
ENDFUNCTION
