' ######################################################### Version 1.00
' ##	Gibt Kalenderblaetter aus als *.fig-Files        ## letzte Bearbeitung:
' ## 	Wochenblaetter			     24.05.2003  ## 25.05.2005
' ############### (c) Markus Hoffmann #####################
'
' Ueberarbeitung 01.2016 MH fuer Version 2.00
'
' * Kalender.bas (c) 2003-2016 by Markus Hoffmann
' *
' * This file is part of Notitzkalender fuer X11-Basic
' * =============================================================
' * Notitzkalender fuer X11-Basic is free software and comes with
' * NO WARRANTY - read the file COPYING/LICENSE for details
' *
'

wochentage$()=["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag","Sonntag"]
monat$()=["Januar","Februar","M\344rz","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember"]

' Vor dem Durchlauf die richtigen Einstellungen hier anpassen:

jahr=VAL(RIGHT$(DATE$,4))    ! Jahr des Kalenders
duplex=TRUE                  ! Duplexdruck?
breite=86                    ! Breite der Felder in mm
hoehe=21                     ! Hoehe der Felder in mm
verbose=0                    ! Schreibe Infos auf die Konsole

IF duplex
  outputfilename$="kalender-duplex.pdf"
ELSE
  outputfilename$="kalender.pdf"
ENDIF

' Kommandozeile bearbeiten
CLR inputfile$,collect$
WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF PARAM$(i)="--help" OR PARAM$(i)="-h"
      @intro
      @using
    ELSE IF PARAM$(i)="--version"
      @intro
      QUIT
    ELSE IF PARAM$(i)="--duplex"
      duplex=TRUE
    ELSE IF PARAM$(i)="--paper"
      INC i
      IF LEN(PARAM$(i))
        paper$=PARAM$(i)
      ENDIF
    ELSE IF PARAM$(i)="-y" OR PARAM$(i)="-j"
      INC i
      IF LEN(PARAM$(i))
        jahr=VAL(PARAM$(i))
      ENDIF
    ELSE IF PARAM$(i)="-d"
      duplex=NOT duplex
    ELSE IF PARAM$(i)="-f"
      figonly=TRUE
    ELSE IF PARAM$(i)="-v"
      INC verbose
    ELSE IF PARAM$(i)="-q"
      DEC verbose
    ELSE IF PARAM$(i)="-o"
      INC i
      IF LEN(PARAM$(i))
        outputfilename$=PARAM$(i)
      ENDIF
    ELSE
      collect$=collect$+PARAM$(i)+" "
    ENDIF
  ELSE
    inputfile$=PARAM$(i)
  ENDIF
  INC i
WEND

' Fixe Feiertage einlesen

DIM feierp$(100),feiern$(100),feiers(100)
anzfeiertage=0
DO
  READ a$
  EXIT IF a$="***"
  feierp$(anzfeiertage)=a$
  READ b$
  READ c
  feiern$(anzfeiertage)=b$
  feiers(anzfeiertage)=c
  INC anzfeiertage
LOOP

' An Ostern gekoppelte Feiertage berechnen

@ostern(jahr)

' Kalenderwoche der ersten Woche im Jahr bestimmen

k=@kw("01.01."+STR$(jahr))

i=0
WHILE k<1
  INC i
  k=@kw(STR$(i,2,2,1)+".01."+STR$(jahr))
WEND
startdate$=STR$(i,2,2,1)+".01."+STR$(jahr)
'  startdate$="29.12.2014"

IF verbose>=0
  PRINT "Beginn des Jahres ";jahr;" mit kw=1 ist der ";startdate$
ENDIF
' Jetzt die einzelnen Seiten ausgeben.

FOR kw=1 TO 52 STEP 4
  @blatt(jahr,kw)
NEXT kw

SYSTEM "psmerge -o"+STR$(jahr)+".ps "+STR$(jahr)+"-*.ps"
SYSTEM "ps2pdf "+STR$(jahr)+".ps "+outputfilename$
SYSTEM "rm -f "+STR$(jahr)+"-*.ps"
IF NOT figonly
  SYSTEM "rm -f "+STR$(jahr)+"-*.fig"
ENDIF
SYSTEM "rm -f "+STR$(jahr)+".ps"
QUIT

PROCEDURE intro
  PRINT "Kalender V.2.00 (c) Markus Hoffmann 2003-2016"
RETURN
PROCEDURE using
  PRINT "Usage: kalender [options] ..."
  PRINT "Options:"
  PRINT "  -h, --help               Display this information"
  PRINT "  -j <Jahr>                Jahreszahl"
  PRINT "  -y <year>                Jahreszahl"
  PRINT "  --duplex                 arrange pages for duplex printing"
  PRINT "  -d                       toggle duplex flag"
  PRINT "  -f                       produce xfig files"
  PRINT "  -o <file>                Place the output into <file>, default: "+outputfile$
  PRINT "  -v                       be more verbose"
  PRINT "  -q                       be more quite"
RETURN

' Erstellt ein Feld mit einem Tag drauf:
' Tag Wochentag
' Mondphase
' Feiertag

PROCEDURE tagfeld(jd,startx,starty)
  LOCAL status,feier$,dayofmonth,vollmond
  LOCAL j,a,nn
  CLR status,vollmond
  FOR j=0 TO anzfeiertage-1
    IF GLOB(JULDATE$(jd),feierp$(j))
      feier$=feiern$(j)
      status=feiers(j)
    ENDIF
  NEXT j

  ' Vollmond finden
  a=0
  nn=(jahr-1900)*12
  WHILE a<jd+14
    a=@mondtag(nn,2)
    INC nn
    ' print nn,juldate$(round(a))
    IF ROUND(a)=jd
      vollmond=TRUE
    ENDIF
  WEND
  dayofmonth=VAL(LEFT$(JULDATE$(jd),2))
  ' Box l
  @ps_box(startx,starty,startx+45*breite,starty+45*hoehe)

  IF verbose>1
    IF status=1 OR ((jd+1) MOD 7)=0
      PRINT "["+STR$(dayofmonth)+"] ";
    ELSE
      PRINT " "+STR$(dayofmonth)+"  ";
    ENDIF
    IF vollmond
      PRINT " O ";
    ELSE
      PRINT " - ";
    ENDIF
    PRINT " "+UPPER$(wochentage$((jd+1) MOD 7))+" ";
    IF status
      PRINT feier$
    ELSE
      PRINT "   ";
    ENDIF
    PRINT
  ENDIF
  IF ((jd+1) MOD 7)=0 OR status=1
    @ps_bcolor(4)
  ELSE IF ((jd+1) MOD 7)=6 OR status=2
    @ps_bcolor(6)
  ELSE
    @ps_bcolor(7)
  ENDIF
  @ps_pbox(startx+45,starty+45,startx+360,starty+270)
  @ps_bcolor(7)
  IF ((jd+1) MOD 7)=0 OR status=1
    PRINT #1,"4 1 7 50 0 18 12 0.0000 4 135 210 "+STR$(startx+180)+" "+STR$(starty+45+180)+" "+STR$(dayofmonth)+"\001"
  ELSE
    PRINT #1,"4 1 0 50 0 18 12 0.0000 4 135 210 "+STR$(startx+180)+" "+STR$(starty+45+180)+" "+STR$(dayofmonth)+"\001"
  ENDIF

  PRINT #1,"4 0 0 50 0 20 12 0.0000 4 135 210 "+STR$(startx+450)+" "+STR$(starty+45+180)+" "+UPPER$(wochentage$((jd+1) MOD 7))+"\001"

  IF status
    PRINT #1,"4 0 0 50 0 20 12 0.0000 4 135 210 "+STR$(startx+450)+" "+STR$(starty+45+180+160)+" "+feier$+"\001"
  ENDIF
  IF vollmond
    PRINT #1,"1 3 0  1 0  7 50 0 20 0.000 1 0.0000 "+STR$(startx+275)+" "+STR$(starty+405)+" "+" 90 90 1755 1575 1870 1575"
  ENDIF
RETURN

' Erstellt ein Feld mit einer Woche drauf:
' Titelzeile [Monat Jahr Woche]
' Linie
' 7 tagesfelder
' Footer (copyright marke)

PROCEDURE woche(j,w,startx,starty)
  LOCAL i,jd,startday,monate,oo
  IF verbose>0
    PRINT "Erstelle Kalenderwoche ";w;" ";j
  ENDIF
  jd=JULIAN(startdate$)+7*w-7
  startday=VAL(LEFT$(JULDATE$(jd),2))
  monate$=""
  oo=-1
  FOR i=0 TO 6
    IF VAL(MID$(JULDATE$(jd+i),4,2))<>oo+1
      oo=VAL(MID$(JULDATE$(jd+i),4,2))-1
      monate$=monate$+monat$(oo MOD 12)+" "
    ENDIF
  NEXT i

  IF verbose>1
    PRINT monate$+" "+STR$(j)+"   Woche "+STR$(w)
    PRINT "--------------------------------------"
  ENDIF
  ' Linie 1
  @ps_line(startx,starty+450,startx+45*breite,starty+450)
  PRINT #1,"4 0 0 50 0 18 12 0.0000 4 150 855 "+STR$(startx+45)+" "+STR$(starty+360)+" "+UPPER$(monate$)+" "+STR$(j)+"\001"
  PRINT #1,"4 2 0 50 0 18 12 0.0000 4 150 840 "+STR$(startx+45*breite)+" "+STR$(starty+360)+" Woche "+STR$(w)+"\001"

  FOR i=0 TO 6
    @tagfeld(jd+i,startx,starty+450+225+i*45*hoehe)
  NEXT i
  PRINT #1,"4 2 0 50 0 18 5 0.0000 4 75 960 "+STR$(startx+45*breite)+" "+STR$(starty+450+225+2*45+45*hoehe*7)+" (c) Markus Hoffmann 2003\001"
RETURN

' Erstellt ein Blatt mit zwei Seiten und jeweils zwei Wochen drauf.
' Vier Wochenfelder in richtige Reihenfolge (duplex oder nicht)
' Linien zum Faltern oder Schneiden
' Linien zum Schneiden
' Markierungen zum Lochen

PROCEDURE blatt(j,w)
  IF verbose>=0
    PRINT "Erstelle Blatt ";w;" ";j
  ENDIF
  jd=JULIAN(startdate$)+7*w-7
  startday=VAL(LEFT$(JULDATE$(jd),2))

  OPEN "O",#1,STR$(j,4,4,1)+"-"+STR$(w,2,2,1)+".fig"
  PRINT #1,"#FIG 3.2"
  PRINT #1,"Portrait"
  PRINT #1,"Center"
  PRINT #1,"Metric"
  PRINT #1,"A4      "
  PRINT #1,"100.00"
  PRINT #1,"Multiple"
  PRINT #1,"-2"
  PRINT #1,"1200 2"
  @ps_bcolor(7)
  ' Box
  offx=1170
  offx=360
  boxw=4365
  offx2=offx+boxw
  boxh=7785
  offy=405
  offy=585
  offy2=offy+297*45
  @ps_dotbox(offx,offy,offx+boxw,offy+boxh)
  @ps_dotbox(offx2,offy,offx2+boxw,offy+boxh)
  @ps_dotbox(offx,offy2,offx+boxw,offy2+boxh)
  @ps_dotbox(offx2,offy2,offx2+boxw,offy2+boxh)

  ' Lochermarkierung
  @ps_line(offx+260,offy+6795,offx+25,offy+6795)
  @ps_line(offx+260,offy2+6795,offx+25,offy2+6795)

  @ps_line(offx+260,offy+5940,offx+25,offy+5940)
  @ps_line(offx+260,offy2+5940,offx+25,offy2+5940)

  @ps_line(offx+260,offy+5085,offx+25,offy+5085)
  @ps_line(offx+260,offy2+5085,offx+25,offy2+5085)

  @ps_line(offx+260,offy+2790,offx+25,offy+2790)
  @ps_line(offx+260,offy2+2790,offx+25,offy2+2790)

  @ps_line(offx+260,offy+1935,offx+25,offy+1935)
  @ps_line(offx+260,offy2+1935,offx+25,offy2+1935)

  @ps_line(offx+260,offy+1080,offx+25,offy+1080)
  @ps_line(offx+260,offy2+1080,offx+25,offy2+1080)

  IF duplex
    @woche(j,w,offx+405,offy+45)
    @woche(j,w+3,offx2+90,offy+45)
    @woche(j,w+2,offx+405,offy2+45)
    @woche(j,w+1,offx2+90,offy2+45)
  ELSE
    @woche(j,w,offx+405,offy+45)
    @woche(j,w+1,offx2+90,offy+45)
    @woche(j,w+2,offx+405,offy2+45)
    @woche(j,w+3,offx2+90,offy2+45)
  ENDIF
  CLOSE #1

  SYSTEM "fig2dev -L ps -M "+STR$(j,4,4,1)+"-"+STR$(w,2,2,1)+".fig"+" > "+STR$(j,4,4,1)+"-"+STR$(w,2,2,1)+".ps"
RETURN

' Fixe Feiertage (Datum-Maske, Name, Art: 1=ges. Feiertag, 2=kein ges. Feiertag, 3=Hinweis)

DATA "01.01.*","Neujahr",1
DATA "06.01.*","Heilige 3 K�nige",3
DATA "20.03.*","Fr�hlingsanfang",3
DATA "01.05.*","Mai-Feiertag",1
DATA "21.06.*","Sommeranfang",3
DATA "15.08.*","Mari� Himmelfahrt",3
DATA "03.10.*","Tag der Deutschen Einheit",1
DATA "31.10.*","Reformationstag",3
DATA "01.11.*","Allerheiligen",3
DATA "24.12.*","Heiligabend",2
DATA "25.12.*","1. Weihnachtstag",1
DATA "26.12.*","2. Weihnachtstag",1
DATA "31.12.*","Silvester",2
DATA "***"

' Berechnet Feiertage, welche an Ostern gekoppelt sind.

PROCEDURE ostern(jahr)
  q=jahr DIV 4
  a=jahr MOD 19
  b=(204-11*a) MOD 30
  IF b=28 OR b=28
    DEC b
  ENDIF
  i=b
  j=jahr+q+i-13
  c=j-(j DIV 7)*7
  o=28+i-c
  IF o<=31
    dd$=STR$(o,2,2,1)+".03."+STR$(jahr,4,4,1)
  ELSE
    dd$=STR$(o-31,2,2,1)+".04."+STR$(jahr,4,4,1)
  ENDIF
  feierp$(anzfeiertage)=dd$
  feiern$(anzfeiertage)="Ostersonntag"
  feiers(anzfeiertage)=1
  INC anzfeiertage
  feierp$(anzfeiertage)=JULDATE$(JULIAN(dd$)+1)
  feiern$(anzfeiertage)="Ostermontag"
  feiers(anzfeiertage)=1
  INC anzfeiertage
  feierp$(anzfeiertage)=JULDATE$(JULIAN(dd$)-2)
  feiern$(anzfeiertage)="Karfreitag"
  feiers(anzfeiertage)=1
  INC anzfeiertage
  feierp$(anzfeiertage)=JULDATE$(JULIAN(dd$)-3)
  feiern$(anzfeiertage)="Gr�ndonnerstag"
  feiers(anzfeiertage)=3
  INC anzfeiertage
  feierp$(anzfeiertage)=JULDATE$(JULIAN(dd$)-46)
  feiern$(anzfeiertage)="Aschermittwoch"
  feiers(anzfeiertage)=3
  INC anzfeiertage
  feierp$(anzfeiertage)=JULDATE$(JULIAN(dd$)-47)
  feiern$(anzfeiertage)="Fastnacht"
  feiers(anzfeiertage)=3
  INC anzfeiertage
  feierp$(anzfeiertage)=JULDATE$(JULIAN(dd$)-48)
  feiern$(anzfeiertage)="Rosenmontag"
  feiers(anzfeiertage)=3
  INC anzfeiertage
  feierp$(anzfeiertage)=JULDATE$(JULIAN(dd$)-52)
  feiern$(anzfeiertage)="Weiberfastnacht"
  feiers(anzfeiertage)=3
  INC anzfeiertage
  feierp$(anzfeiertage)=JULDATE$(JULIAN(dd$)+39)
  feiern$(anzfeiertage)="Christi Himmelfahrt"
  feiers(anzfeiertage)=1
  INC anzfeiertage
  feierp$(anzfeiertage)=JULDATE$(JULIAN(dd$)+49)
  feiern$(anzfeiertage)="Pfingstsonntag"
  feiers(anzfeiertage)=1
  INC anzfeiertage
  feierp$(anzfeiertage)=JULDATE$(JULIAN(dd$)+50)
  feiern$(anzfeiertage)="Pfingstmontag"
  feiers(anzfeiertage)=1
  INC anzfeiertage
  feierp$(anzfeiertage)=JULDATE$(JULIAN(dd$)+60)
  feiern$(anzfeiertage)="Fronleichnam"
  feiers(anzfeiertage)=3
  INC anzfeiertage
RETURN

' Berechnet viertel-Mondphasen ab julianischem Tag n

' nph=0  Neumond
' nph=1  1. viertel
' nph=2  Vollmond
' nph=3  letztes viertel

' Rueckgabe ist julianischer tag

FUNCTION mondtag(n,nph)
  LOCAL c,t,t2,as,am,jd
  c=n+nph/4
  t=c/1236.85
  as=359.2242+29.105356*c+((1.178e-4)-(1.55e-7)*t)*t^2
  am=306.0253+385.816918*c+0.010730*t^2
  jd=2415020+28*n+7*nph+0.75933+1.53058868*c
  IF nph=0 OR nph=2
    ADD jd,(0.1734-3.93e-4*t)*SIN(RAD(as))-0.4068*SIN(RAD(am))
  ELSE IF nph=1 OR nph=3
    ADD jd,(0.1721-4e-4*t)*SIN(RAD(as))-0.6280*SIN(RAD(am))
  ELSE
    RETURN 0
  ENDIF
  RETURN jd
ENDFUNCTION

' Gibt Kalenderwoche zu Datum (c) Markus Hoffmann
FUNCTION kw(d$)
  LOCAL j,j2,wt,wt2,a,b
  j=JULIAN(d$)
  wt=(j+7) MOD 7
  a=j-wt+3   ! Donnerstag der Woche
  j2=JULIAN("04.01."+RIGHT$(d$,4))
  wt2=(j2+7) mod 7
  b=j2-wt2+3 ! Erster Donnerstag im Jahr
  RETURN (a-b)/7+1
ENDFUNCTION

' Hier nun die Zeichenfunktionen (Xfig 3.2)
'
' Koordinatenumrechnung fuer Millimeter
PROCEDURE mmm
  MUL x1,45
  MUL y1,45
  MUL x2,45
  MUL y2,45
  y1=INT(y1)
  y2=INT(y2)
  x1=INT(x1)
  x2=INT(x2)
RETURN

' Attribute fuer Linien, Text etc...
PROCEDURE ps_bcolor(c)
  bcolorn=c
RETURN
PROCEDURE ps_dotbox(x1,y1,x2,y2)
  PRINT #1,"2 2 2 1 0 7 50 0 -1 3.000 0 0 -1 0 0 5"
  PRINT #1,"	 "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y2)+" "+STR$(x1)+" "+STR$(y2)+" "+STR$(x1)+" "+STR$(y1)
RETURN
PROCEDURE ps_box(x1,y1,x2,y2)
  PRINT #1,"2 2 0 1 0 "+STR$(bcolorn)+" 50 0 -1 0.000 0 0 -1 0 0 5"
  PRINT #1,"	 "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y2)+" "+STR$(x1)+" "+STR$(y2)+" "+STR$(x1)+" "+STR$(y1)
RETURN
PROCEDURE ps_pbox(x1,y1,x2,y2)
  PRINT #1,"2 1 0 1 0 "+STR$(bcolorn)+" 50 0 20 0.000 0 0 -1 0 0 5"
  PRINT #1,"	 "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y2)+" "+STR$(x1)+" "+STR$(y2)+" "+STR$(x1)+" "+STR$(y1)
RETURN
PROCEDURE ps_line(x1,y1,x2,y2)
  PRINT #1,"2 1 0 1 0 7 50 -1 -1 0.000 0 0 -1 0 0 2"
  PRINT #1,"	 "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y2)
RETURN
