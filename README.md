X11-Basic example programs
==========================

This is a collection of example programs (.bas source files) for X11-Basic --
the BASIC interpreter  and compiler for LINUX/UNIX, Android, TomTom and MS
WINDOWS by Markus Hoffmann

Maintainer of the collection: Markus Hoffmann 2002-2020

Name: X11-Basic_examples
Version: 1.27
Copyright: GPL or public domain if not stated otherwise in the code
Packager: Markus Hoffmann <kollo@users.sourceforge.net>

You can submit your programs by email to kollo@users.sourceforge.net, if you
want to see them included here (or do a merge request on this repository).

For information about X11-Basic, the BASIC language please refer to
http://x11-basic.sourceforge.net/


Description
-----------


Thease are example programs for X11-Basic. Most of them can be easily run from 
a text console, e.g. on UNIX/Linux systems. Not all of them are useful on 
other platforms like WINDOWS or Android.

All
===
Example Programs which should run on all platforms.

All/Tests
---------
Little programs which demonstrate the use of certain X11-Basic commands and
can be used to play with them and to debug the Interpreter.

All/GPS-Earth
-------------
This is the package GPS-Earth for GARMIN ETREX GPS receivers. The program was 
created to run under Linux. GPS-Earth has its own Homepage on sourceforge.

All/fragments
-------------
These collection consists of incomplete fragments of Programs which have not 
been finished or not yet fully been converted to X11-Basic. These are just 
ment as inspirations in case someone wants to finish them.



Android-only
============
Example Programs which are known to run well on Android.

Linux-only
==========
Example Programs which only run on linux

TomTom-only
===========
Example programs which only make sense on TomTom devices.
Programs which have been written explicitly for X11-Basic on TomTom devices.
They make use of specific features like the GPS environment of the Tomtom, which
are not available on other platforms. So they will not work correctly if run on
other devices.  


WINDOWS-only
============

We currently do not have programs which only run on WINDOWS.


Have fun.

regards Markus Hoffmann

Authors:
--------
    Markus Hoffmann <kollo@users.sourceforge.net>


    All example programs are free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    All programs are distributed in the hope that they will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
